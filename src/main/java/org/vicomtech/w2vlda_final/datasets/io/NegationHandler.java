package org.vicomtech.w2vlda_final.datasets.io;

import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Sets;

/**
 * A helper class to apply negations in a windows (attaching a prefix to words near a negation particle).
 * A naive approach commonly used in the literature, just for testing.
 * @author yo
 *
 */
public class NegationHandler {

	private static final Set<String>NEGATIONS_EN=Sets.newHashSet("not","no","never");
	private static final Set<String>NEGATIONS_ES=Sets.newHashSet("no","ni","poco","poca");
	private static final Set<String>NEGATIONS_IT=Sets.newHashSet("no","né","nulla");
	private static final Set<String>NEGATIONS_FR=Sets.newHashSet("non");
	private static final Set<String>NEGATIONS_RU=Sets.newHashSet("нет");
	private static final Set<String>NEGATIONS_NL=Sets.newHashSet("nee","geen","nulla");
	private static final Set<String>NEGATIONS_TR=Sets.newHashSet("hayır");
	
	private final Set<String>negations;
	
	public NegationHandler() {
		this.negations=NEGATIONS_EN;
	}
	
	public NegationHandler(String lang){
		this.negations=Sets.newHashSet();
		switch (lang) {
		case "en":
			this.negations.addAll(NEGATIONS_EN);
			break;
		case "es":
			this.negations.addAll(NEGATIONS_ES);
			break;
		case "it":
			this.negations.addAll(NEGATIONS_IT);
			break;
		case "fr":
			this.negations.addAll(NEGATIONS_FR);
			break;
		case "nl":
			this.negations.addAll(NEGATIONS_NL);
			break;
		case "ru":
			this.negations.addAll(NEGATIONS_RU);
			break;
		case "tr":
			this.negations.addAll(NEGATIONS_TR);
			break;
		default:
			throw new RuntimeException("Unexpected lang value for negations: "+lang);
		}
	}
	
	public String processNegation(String line){
		//this is just a clean-up for old negation marks if they exist and some ad-hoc hotfixes
		String[] tokens=line.replace("NOT#", "").replace("n ' t", "not").split(" ");
		int negationScope=4;
		for(int i=0;i<tokens.length;i++){
			String token=tokens[i].trim();
			if(isNegation(token)){
				for(int j=i;j<Math.min(i+negationScope, tokens.length);j++){
					tokens[j]="not_"+tokens[j];
				}
				i+=negationScope-1;//the -1 is because i is going to be increased and it was skipping the first token after the negation scope
			}
		}
		return StringUtils.join(tokens," ");
	}
	
	public boolean isNegation(String word){
		return negations.contains(word);
	}
	
}
