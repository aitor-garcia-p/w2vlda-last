package org.vicomtech.w2vlda_final.datasets.io;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vicomtech.w2vlda_final.utils.MyFileUtils;

import com.google.common.collect.Lists;

/**
 * The aim of this class is to read the actual content, stripping ids or gold
 * labels. For example: from ID_DOC TAB CONTENT TAB GOLD_LABELS -> CONTENT This
 * is useful for the components that read just plain and clean text, like the
 * word2vec generator
 * 
 * @author agarciap
 *
 */
public class PlainContentDatasetParser {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlainContentDatasetParser.class);

	private static final String SEPARATOR = "\t";

	public static List<String> getPlainContentLines(String pathToDataset) {
		LOGGER.info("Reading plain content dataset from {}", pathToDataset);
		List<String> lines = MyFileUtils.readLines(pathToDataset);
		List<String> resultingLines = Lists.newArrayList();
		for (String line : lines) {
			if (line.trim().length() > 0) {
				try {
					resultingLines.add(line.split(SEPARATOR)[1]);
				} catch (ArrayIndexOutOfBoundsException e) {
					LOGGER.error("Error parsing dataset line number {}: {}", lines.indexOf(line), line);
					throw new RuntimeException(e);
				}
			}
		}
		return resultingLines;
	}

	public static List<String[]> getWhitespaceTokenizedPlainContent(String pathToDataset) {
		List<String> lines = MyFileUtils.readLines(pathToDataset);
		List<String[]> resultingLines = Lists.newArrayList();
		for (String line : lines) {
			resultingLines.add(line.split(SEPARATOR)[1].trim().replaceAll("\\s+", " ").split(" "));
		}
		return resultingLines;
	}

	public static String getPathToTemporalFileWithPlainContentDataset(String pathToDataset, boolean lowerCaseText) {
		try {
			File temp = File.createTempFile("aaa", "temp");
			temp.deleteOnExit();
			List<String> plainLines = getPlainContentLines(pathToDataset);
			if (lowerCaseText) {
				plainLines = plainLines.stream().map(x -> x.toLowerCase()).collect(Collectors.toList());
			}
			MyFileUtils.writeLines(plainLines, temp.getAbsolutePath());
			return temp.getAbsolutePath();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

}
