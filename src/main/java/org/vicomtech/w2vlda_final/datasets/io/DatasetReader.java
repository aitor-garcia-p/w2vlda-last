package org.vicomtech.w2vlda_final.datasets.io;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;
import org.vicomtech.w2vlda_final.datasets.state.Polarity;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.datasets.state.Word;
import org.vicomtech.w2vlda_final.utils.TextBasicPreprocess;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * A class that reads the datasets.
 * The expected format is:
 * docId TAB white-space-separated-tokens
 * 
 * The vocabulary that is used to compute the topic model is built here, from the words present in the read documents
 * @author yo
 *
 */
public class DatasetReader {

	private static final Logger logger=LoggerFactory.getLogger(DatasetReader.class);
	
	@Value("${w2vlda.min_doc_length:5}")
	private int minDocSize=5;
	@Value("${w2vlda.max_doc_length:30}")
	private int maxDocSize=30;
	@Value("${w2vlda.cutoffThreshold:10}")
	private int cutoffThreshold=10;
	
	@Autowired
	private Vocabulary vocabulary;
	@Autowired
	private StopwordsHandler stopwordsHandler;
	
	private static final String FIELDS_SEPARATOR="\t";
	private static final String GOLD_CATEGORIES_SEPARATOR=",";
	
	
	public Dataset readDataset(String datasetPath) {
		Dataset dataset=new Dataset();
		List<String>lines=loadLines(datasetPath);
		Set<String> wordsPassingCut = calculateWordsAboveMinSupport(lines);
		logger.info("Vocabulary content size before loading the dataset: {}",vocabulary.size());
		for(String line:lines){
			if(nonEmptyLine(line)){
				//Split the line
				String[]lineFields=line.split(FIELDS_SEPARATOR);
				//Retrieve the first two mandatory fields
				String internalDocId=lineFields[0];
				String textContent=lineFields[1];
				//create the document instance
				int docId=(int) dataset.size();
				Document document=new Document(docId);
				document.setDocInternalId(internalDocId);
				//Parse text content and add to the instance
				String[]tokens=processLine(textContent);
				for(String token:tokens){
					if(isStopword(token) || !wordsPassingCut.contains(token)){
						Word word=Word.createStopword(document, -1, token);
						document.addWord(word);
					}else{
						Word word=Word.createWord(document, vocabulary.addWord(token), token);
						document.addWord(word);
					}
				}
				//Check the rest of the fields (optional) and parse accordingly
				//Gold polarity annotation
				if(lineFields.length>2){
					Polarity goldPolarity=Polarity.parse(lineFields[2]);
					document.setGoldPolarity(goldPolarity);
				}
				//Gold categories
				if(lineFields.length>3){
					String[]goldCategories=parseGoldCategoriesIfAny(lineFields[3]);
					for(String goldCategory:goldCategories){
						document.addGoldCategory(goldCategory);
					}
				}
				//If document is accepted add to dataset
				if(acceptDocument(document)){
					dataset.addDocument(document);
				}
			}
		}
		logger.info("Vocabulary content size after loading the dataset: {}",vocabulary.size());
		return dataset;
	}

	/**
	 * Gold categories should be at the end separated from the line content.
	 * The categories themselves have a particular separator among them
	 * @param line
	 * @return
	 */
	protected String[]parseGoldCategoriesIfAny(String goldCategoriesStr){
		String[]goldCategories=goldCategoriesStr.split(GOLD_CATEGORIES_SEPARATOR);
		for(int i=0;i<goldCategories.length;i++){
			goldCategories[i]=goldCategories[i].trim();
		}
		return goldCategories;
	}
	
	/**
	 * This should work when having gold categories and when not
	 * @param line
	 * @return
	 */
	protected String removeGoldCategoriesFromLine(String line){
		return line.split(FIELDS_SEPARATOR)[0];
	}
	
	protected String[] processLine(String textContent) {
		boolean removeTildes=true;
		String processedLine=TextBasicPreprocess.preprocess(textContent,removeTildes);
		return processedLine.split(" ");
	}

	protected boolean isStopword(String token) {
		return stopwordsHandler.isStopword(token);
	}

	protected boolean nonEmptyLine(String line) {
		return !line.trim().isEmpty();
	}
	
	/**
	 * Accepts a document if it meets some conditions. For now, its number of words must be in a certain range (>4 and <30 by default)
	 * @param document
	 * @return
	 */
	protected boolean acceptDocument(Document document){
		return document.size()<maxDocSize && document.size()>minDocSize;
	}
	
	private List<String>loadLines(String path){
		try {
			return FileUtils.readLines(new File(path), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException("Problem loading dataset",e);
		}
	}
	
	public void setStopwordsHandler(StopwordsHandler stopwordsHandler){
		this.stopwordsHandler=stopwordsHandler;
	}

	public void setMinDocSize(int minDocSize) {
		this.minDocSize = minDocSize;
	}

	public void setMaxDocSize(int maxDocSize) {
		this.maxDocSize = maxDocSize;
	}

	public void setVocabulary(Vocabulary vocabulary) {
		this.vocabulary = vocabulary;
	}
	
	private Set<String>calculateWordsAboveMinSupport(List<String>lines){
		logger.info("Calculating word (different tokens) frequencies in the dataset...");
		Map<String,Integer>wordCount=Maps.newHashMap();
		for(String line:lines){
			String[]tokens=processLine(line);
			for(String token:tokens){
				int count=wordCount.getOrDefault(token, 0);
				count++;
				wordCount.put(token, count);
			}
		}
		logger.info("Number of different words (different tokens) before filtering: {}",wordCount.size()); 
		logger.info("Creating filter for low frequency word, cut-off: {}",cutoffThreshold);
		Set<String>wordsPassingCut=Sets.newHashSet();
		//Integer threshold=200;
		for(String word:wordCount.keySet()){
			if(wordCount.get(word)>cutoffThreshold){
				wordsPassingCut.add(word);
			}
		}
		logger.info("Number of different words (different tokens) after filtering: {}",wordsPassingCut.size()); 
		return wordsPassingCut;
	}
	
	
}
