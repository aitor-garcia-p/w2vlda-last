package org.vicomtech.w2vlda_final.datasets.io;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.collect.Sets;

/**
 * A helper class to deal with stopwords.
 * Stopwords are 
 * @author yo
 *
 */
public class StopwordsHandler {

	private static final Logger LOGGER=LoggerFactory.getLogger(StopwordsHandler.class);
	
	@Value("${w2vlda.stopwords:classpath:english_stopwords.txt}")
	private String stopwordsPath;
	
	private Set<String>stopwords;
	
	
	/**
	 * Convenience static factory method
	 * @param lang
	 * @return
	 */
	public static StopwordsHandler getStopwordHandlerFor(String lang) {
		switch (lang) {
		case "en":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("english_stopwords.txt").getPath());
		case "es":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("spanish_stopwords.txt").getPath());
		case "it":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("italian_stopwords.txt").getPath());
		case "fr":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("french_stopwords.txt").getPath());
		case "de":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("german_stopwords.txt").getPath());
		case "nl":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("dutch_stopwords.txt").getPath());
		case "ar":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("arabic_stopwords.txt").getPath());
		case "ru":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("russian_stopwords.txt").getPath());
		case "tu":
			return new StopwordsHandler(
					StopwordsHandler.class.getClassLoader().getResource("turkish_stopwords.txt").getPath());
		default:
			throw new RuntimeException("No default stopword handler for this language: " + lang);
		}

	}
	
	/**
	 * Default constructor, empty stopwords list, but still filters punctuation
	 */
	public StopwordsHandler() {
		this.stopwords = Sets.newHashSet();
	}

	/**
	 * New stopword handler that will load a stopwords list from the given path,
	 * and will filter also punctuation
	 * 
	 * @param stopwordsPath
	 */
	public StopwordsHandler(String stopwordsPath) {
		loadStopwords(stopwordsPath);
	}
	
	public void init(){
		loadStopwords(stopwordsPath);
	}
	
	public boolean isStopword(String token) {
		return stopwords.contains(token.replace("not_", "")) || token.matches("\\p{Punct}+");
	}
	
	private void loadStopwords(String stopwordsPath){
		LOGGER.info("Loading stopwords from: {}",stopwordsPath);
		stopwords=Sets.newHashSet();
		try {
			List<String> stopwordsLines;
			if (stopwordsPath.startsWith("classpath:")) {
				stopwordsLines = IOUtils.readLines(ClassLoader.getSystemResourceAsStream(stopwordsPath.split(":")[1]),
						StandardCharsets.UTF_8);
			} else {
				stopwordsLines = FileUtils.readLines(new File(stopwordsPath), StandardCharsets.UTF_8);
			}
			List<String> processedStopwordLines = stopwordsLines.stream().map(x -> x.trim()).filter(x -> !x.startsWith("#")).collect(Collectors.toList());
			this.stopwords = Sets.newHashSet();
			stopwords.addAll(processedStopwordLines);
			LOGGER.info("Loaded {} stopwords",processedStopwordLines.size());
		} catch (Exception e) {
			throw new RuntimeException("Problem loading stopwords", e);
		}
	}
	
	public void setStopwordsPath(String stopwordsPath) {
		this.stopwordsPath = stopwordsPath;
	}
}
