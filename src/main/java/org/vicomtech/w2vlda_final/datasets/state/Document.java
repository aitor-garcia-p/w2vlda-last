package org.vicomtech.w2vlda_final.datasets.state;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class Document {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(Document.class);

	private List<Word> words = Lists.newArrayList();
	private List<Integer> wordIds = Lists.newArrayList();
	private Map<Integer, Integer> wordFrequencies = Maps.newHashMap();

	// This id is incremental, and serves to point to its position in the LDA
	// count matrix
	private int docId;
	// This id is internal, parsed from the input dataset file, and serves for
	// generic identification
	private String docInternalId = "N/A";
	private int sentenceNum = 0;

	private int topic = -1;
	private int sentiment = -1;

	////
	// Gold categories/topics (to be read from a gold-standard, for evaluation
	//// purposes)
	private Set<String> goldCategories = Sets.newHashSet();
	private Polarity goldPolarity=Polarity.UNASSIGNED;
	////

	public Document(int docId) {
		this.docId = docId;
	}

	public void addWord(Word word) {
		words.add(word);
		wordIds.add(word.getId());
		increaseCurrentWordCount(word);
	}

	private void increaseCurrentWordCount(Word word) {
		int wordId = word.getId();
		int currentFreq = wordFrequencies.getOrDefault(wordId, 0);
		currentFreq++;
		wordFrequencies.put(wordId, currentFreq);
	}

	public List<Word> getWords() {
		return words;// ImmutableList.copyOf(words);
	}

	public List<Integer> getWordIds() {
		return wordIds;// ImmutableList.copyOf(wordIds);
	}

	public Map<Integer, Integer> getDocumentWordFrequencies() {
		return wordFrequencies;// ImmutableMap.copyOf(wordFrequencies);
	}

	public int getTopic() {
		return topic;
	}

	public void setTopic(int topic) {
		this.topic = topic;
	}

	public int getSentiment() {
		return sentiment;
	}

	public void setSentiment(int sentiment) {
		this.sentiment = sentiment;
	}

	public double size() {
		return words.size();
	}

	public int getDocId() {
		return docId;
	}

	@Override
	public String toString() {
		return StringUtils.join(this.words.stream().map(x -> x.getWordform().replace("not_", "")).collect(Collectors.toList()), " ");
	}

	public void addGoldCategory(String goldCategory) {
		this.goldCategories.add(goldCategory);
	}

	public Set<String> getGoldCategories() {
		return goldCategories;// ImmutableSet.copyOf(this.goldCategories);
	}

	public Polarity getGoldPolarity() {
		return goldPolarity;
	}

	public void setGoldPolarity(Polarity goldPolarity) {
		this.goldPolarity = goldPolarity;
	}

	public String getDocInternalId() {
		return docInternalId;
	}

	/**
	 * Parses id (which has to be in XXX_sentenceNum format) and assigns the values to the corresponding variables
	 * @param docInternalId
	 */
	public void setDocInternalId(String docInternalId) {
		String[]idAndSentenceNum=docInternalId.split("_");
		this.docInternalId = docInternalId.substring(0,docInternalId.lastIndexOf('_'));//idAndSentenceNum[0];
		this.sentenceNum=Integer.parseInt(idAndSentenceNum[idAndSentenceNum.length-1]);
	}

	public int getSentenceNum() {
		return sentenceNum;
	}

}
