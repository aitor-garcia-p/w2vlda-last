package org.vicomtech.w2vlda_final.datasets.state;

import java.util.List;

import com.google.common.collect.Lists;

public class Dataset {

	private List<Document>documents=Lists.newArrayList();
	
	public void addDocument(Document document){
		if(documents==null){
			documents=Lists.newArrayList();
		}
		documents.add(document);
	}
	
	public List<Document>getDocuments(){
		return documents;//ImmutableList.copyOf(documents);
	}
	
	public double size(){
		return documents.size();
	}
	
}
