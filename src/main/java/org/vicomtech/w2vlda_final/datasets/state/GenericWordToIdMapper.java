package org.vicomtech.w2vlda_final.datasets.state;

import java.io.Serializable;
import java.util.Map;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableMap;

public class GenericWordToIdMapper implements Serializable{

	private static final long serialVersionUID = 1L;

	private BiMap<String, Integer>dictionary=HashBiMap.create();
	private int currentId=0;
	
	public int addElement(String element){
		if (!dictionary.containsKey(element)) {
			dictionary.put(element, currentId);
			currentId++;
		}
		return dictionary.get(element);
	}
	
	public int getId(String element){
		if(dictionary.containsKey(element)){
			return dictionary.get(element);
		}else{
			return defaultBehaviourWhenElementNotPresent(element);
		}
	}

	private int defaultBehaviourWhenElementNotPresent(String element) {
		//return -1;//this could also be raising an [unchecked] exception
		return addElement(element);
	}
	
	public String getElement(int id){
		if(dictionary.inverse().containsKey(id)){
			return dictionary.inverse().get(id);
		}else{
			return defaultBehaviourWhenIdNotPresent(id);
		}
	}
	
	public boolean contains(String word){
		return dictionary.containsKey(word);
	}
	
	private String defaultBehaviourWhenIdNotPresent(int id) {
		throw new RuntimeException("No element with id: "+id);//it could do another thing
	}
	
	public int size(){
		return dictionary.size();
	}
	
	/**
	 * Returns a immutable map with the current state of the internal dictionary
	 * @return
	 */
	public Map<String,Integer>getDictionary(){
		return ImmutableMap.copyOf(dictionary);
	}
	
	/**
	 * Dumps the given map into the internal dictionary
	 * @param dict
	 */
	public void setDictionary(Map<String,Integer>dict){
		this.dictionary.putAll(dict);
	}
	
}
