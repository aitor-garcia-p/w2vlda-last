package org.vicomtech.w2vlda_final.datasets.state;


import java.io.Serializable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The vocabulary that hold the mapping word <-> wordId
 * The integer for each word is used to index it in the matrix structures during the modelling
 * @author yo
 *
 */
public class Vocabulary implements Serializable{

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER=LoggerFactory.getLogger(Vocabulary.class);
	
	private GenericWordToIdMapper dictionary=new GenericWordToIdMapper();
	
	public Vocabulary(){
		super();
		LOGGER.info("Instantiating Vocabulary...");
	}
	
	public int addWord(String word){
		return dictionary.addElement(word);
	}
	
	public int getId(String word){
		return dictionary.getId(word);
	}
	
	public String getWord(int id){
		return dictionary.getElement(id);
	}
	
	public boolean contains(String word){
		return dictionary.contains(word);
	}
	
	public int size(){
		return dictionary.size();
	}
	
	/**
	 * Returns a immutable map with the current state of the internal dictionary
	 * @return
	 */
	public Map<String,Integer>getDictionary(){
		return dictionary.getDictionary();
	}
	
	/**
	 * Dumps the given map into the internal dictionary
	 * @param dict
	 */
	public void setDictionary(Map<String,Integer>dict){
		dictionary.setDictionary(dict);
	}
	
}
