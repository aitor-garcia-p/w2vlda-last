package org.vicomtech.w2vlda_final.datasets.state;

public enum Polarity {

	POSITIVE,NEGATIVE,NEUTRAL,UNASSIGNED;
	
	public static Polarity parse(String polarityStr){
		switch (polarityStr.toUpperCase()) {
		case "POSITIVE":
		case "5.0":
		case "4.0":
		case "5":
		case "4":
			return POSITIVE;
		case "2.0":
		case "1.0":
		case "2":
		case "1":
		case "NEGATIVE":
			return NEGATIVE;
		case "NEUTRAL":
			return NEUTRAL;
		case "N/A":
			return UNASSIGNED;
			//IGNORE CONFLICT POLARITY
		case "CONFLICT":
			return UNASSIGNED;
		default:
			throw new RuntimeException("Invalid polarity string: "+polarityStr);
		}
	}
	
}
