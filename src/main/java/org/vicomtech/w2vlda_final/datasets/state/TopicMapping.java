package org.vicomtech.w2vlda_final.datasets.state;

import java.io.Serializable;
import java.util.Map;

public class TopicMapping implements Serializable{

	private static final long serialVersionUID = 1L;
	private GenericWordToIdMapper dictionary=new GenericWordToIdMapper();
	
	public int getTopicNumber(String topicName){
		return dictionary.getId(topicName);
	}
	
	public String getTopicName(int topicNumber){
		return dictionary.getElement(topicNumber);
	}
	
	public double getNumTopics(){
		return dictionary.size();
	}
	
	/**
	 * Returns a immutable map with the current state of the internal dictionary
	 * @return
	 */
	public Map<String,Integer>getDictionary(){
		return dictionary.getDictionary();
	}
	
	/**
	 * Dumps the given map into the internal dictionary
	 * @param dict
	 */
	public void setDictionary(Map<String,Integer>dict){
		this.dictionary.setDictionary(dict);
	}
	
}
