package org.vicomtech.w2vlda_final.datasets.state;

public class Word {

	private final Document parent;
	private final int id;
	private final String wordform;
	////////////////////
	private boolean isStopword = false;
	private boolean isOOV = false;
	////////////////////
	private int topic = -1;
	private int wordType = -1;
	private int polarity = -1;

	public static Word createWord(Document parent, int id, String wordform) {
		return new Word(parent, id, wordform, false, false);
	}

	public static Word createStopword(Document parent, int id, String wordform) {
		return new Word(parent, -1, wordform, true, false);
	}

	public static Word createOOV(Document parent, int id, String wordform) {
		return new Word(parent, id, wordform, false, true);
	}

	private Word(Document parent, int id, String wordform, boolean isStopword, boolean isOOV) {
		super();
		this.parent = parent;
		this.id = id;
		this.wordform = wordform;
		this.isStopword = isStopword;
		this.isOOV = isOOV;
	}

	public Document getParent() {
		return parent;
	}

	public int getId() {
		return id;
	}

	public String getWordform() {
		return wordform;
	}

	@Override
	public String toString() {
		return getWordform();
	}

	public int getTopic() {
		return topic;
	}

	public void setTopic(int topic) {
		// System.err.println("TOPIC IS BEING SET! to: "+topic);
		this.topic = topic;
	}

	public int getPolarity() {
		return polarity;
	}

	public void setPolarity(int polarity) {
		this.polarity = polarity;
	}

	public int getWordType() {
		return wordType;
	}

	public void setWordType(int wordType) {
		this.wordType = wordType;
	}

	public boolean isStopword() {
		return isStopword;
	}

	public boolean isOOV() {
		return isOOV;
	}

}
