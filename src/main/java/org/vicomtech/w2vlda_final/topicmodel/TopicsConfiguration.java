package org.vicomtech.w2vlda_final.topicmodel;

import java.io.Serializable;
import java.util.List;

public class TopicsConfiguration implements Serializable{


	private static final long serialVersionUID = 1L;
	private List<String> generalPositives;
	private List<String> generalNegatives;
	private List<Topic> topics;

	public List<String> getGeneralPositives() {
		return generalPositives;
	}

	public void setGeneralPositives(List<String> generalPositives) {
		this.generalPositives = generalPositives;
	}

	public List<String> getGeneralNegatives() {
		return generalNegatives;
	}

	public void setGeneralNegatives(List<String> generalNegatives) {
		this.generalNegatives = generalNegatives;
	}

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	@Override
	public String toString() {
		return "TopicsConfiguration [generalPositives=" + generalPositives + ", generalNegatives=" + generalNegatives
				+ ", topics=" + topics + "]";
	}

	public static class Topic implements Serializable{

		private static final long serialVersionUID = 1L;
		private String topicName;
		private List<String> aspectTerms;
		private List<String> positives;
		private List<String> negatives;

		public String getTopicName() {
			return topicName;
		}

		public void setTopicName(String topicName) {
			this.topicName = topicName;
		}

		public List<String> getAspectTerms() {
			return aspectTerms;
		}

		public void setAspectTerms(List<String> aspectTerms) {
			this.aspectTerms = aspectTerms;
		}

		public List<String> getPositives() {
			return positives;
		}

		public void setPositives(List<String> positives) {
			this.positives = positives;
		}

		public List<String> getNegatives() {
			return negatives;
		}

		public void setNegatives(List<String> negatives) {
			this.negatives = negatives;
		}

		@Override
		public String toString() {
			return "Topic [topicName=" + topicName + ", aspectTerms=" + aspectTerms + ", positives=" + positives
					+ ", negatives=" + negatives + "]";
		}
	}

}
