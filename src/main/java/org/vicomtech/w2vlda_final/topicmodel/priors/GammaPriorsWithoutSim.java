package org.vicomtech.w2vlda_final.topicmodel.priors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;
import org.vicomtech.w2vlda_final.datasets.state.Word;
import org.vicomtech.w2vlda_final.embeddings.WordVectors;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.estimation.Estimator;

public class GammaPriorsWithoutSim extends GammaPriors{

	private static final Logger LOGGER=LoggerFactory.getLogger(GammaPriorsWithoutSim.class);
	
	private static final int BIG_NUMBER_FOR_PRIOR_BIAS=1000;
	
	@Value("${w2vlda.baseGamma}")
	private double baseGamma;
	
	@Autowired
	private Dataset dataset;
	@Autowired
	private TopicsConfiguration topicConfiguration;
	@Autowired @Qualifier("domainWordVectors")
	private WordVectors domainWordVectors;
	
	//State variables
	private double[][]gammas;
	//private double[]gammaSums;
	
	public void init(){
		LOGGER.info("Init gamma priors (base gammaPrior={})",baseGamma);
		gammas=new double[dataset.getDocuments().size()][topicConfiguration.getTopics().size()];//Maps.newHashMap();
		for (Document document : dataset.getDocuments()) {
			processDocument(document);
		}
	}
	
	private void processDocument(Document document) {
		int docId=document.getDocId();
		for(Word word:document.getWords()){
			if(word.isStopword()){
				continue;
			}
			gammas[docId][Estimator.POSITIVE_INDEX_IN_DOCPOLARITY]+=topicConfiguration.getGeneralPositives().contains(word)?BIG_NUMBER_FOR_PRIOR_BIAS:baseGamma;
			gammas[docId][Estimator.NEGATIVE_INDEX_IN_DOCPOLARITY]+=topicConfiguration.getGeneralNegatives().contains(word)?BIG_NUMBER_FOR_PRIOR_BIAS:baseGamma;

		}
		
		//gammas[docId]=normalizeToZeroOne(gammas[docId]);
		
		//normalize and multiply base prior
//		for(int i=0;i<Estimator.NUM_POLARITY_TYPES;i++){
//				//topicPriors[i]/=totalMass;
//			//gammas[docId][i]/=numberOfNonStopwords;//normalizing by the document length (gives a range 0-1, but not a sum-to-one among topics)
//			//priors[docId][i]+=0.5;//to displace the range to 0.5-1.5 so base alpha can be smoothed or powered
//			gammas[docId][i]*=baseGamma;
//		}
		//priors.put(docId, polarityPriors);
		
		//normalizeValues(priors);
	}

	public double getGamma(int docId, int polarity) {
		return gammas[docId][polarity];//priors.get(docId)[polarity];
	}
	
	public double getBaseGamma(){
		return this.baseGamma;
	}
	
//	/**
//	 * Only to be used for inference, i.e. dataset is the new set of unseen documents
//	 * @param dataset
//	 */
//	public void setDatasetForInference(Dataset dataset){
//		this.dataset=dataset;
//	}
	
//	private double[] normalizeToZeroOne(double[]values){
//		double min=Double.MAX_VALUE;
//		double max=Double.MIN_VALUE;
//		for(int i=0;i<values.length;i++){
//			min=Math.min(min, values[i]);
//			max=Math.max(max, values[i]);
//		}
//		double[]normalizedValues=new double[values.length];
//		for(int i=0;i<values.length;i++){
//			normalizedValues[i]=(values[i]-min+0.01)/(max-min+0.01);//note the smoothing value to prevent problems with NaNs
//		}
////		LOGGER.info("Normalizing values, original values: {}  normalized values: {}",values,normalizedValues);
//		return normalizedValues;
//	}
	
}
