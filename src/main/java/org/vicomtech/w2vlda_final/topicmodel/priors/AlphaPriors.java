package org.vicomtech.w2vlda_final.topicmodel.priors;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;
import org.vicomtech.w2vlda_final.datasets.state.TopicMapping;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.datasets.state.Word;
import org.vicomtech.w2vlda_final.embeddings.WordVectors;
import org.vicomtech.w2vlda_final.embeddings.vector_utils.VectorSimilarityCalculator;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration.Topic;

import com.google.common.collect.Lists;

public class AlphaPriors {

	private static final Logger LOGGER=LoggerFactory.getLogger(AlphaPriors.class);
	
	@Value("${w2vlda.baseAlpha}")
	private double baseAlpha;
	@Value("${w2vlda.generalDomainProportion:0.5}")
	private double generalDomainProportion;
	
	@Autowired
	private Dataset dataset;
	@Autowired
	private TopicsConfiguration topicConfiguration;
	@Autowired @Qualifier("domainWordVectors")
	private WordVectors domainWordVectors;
	@Autowired @Qualifier("generalWordVectors")
	private WordVectors generalWordVectors;
	@Autowired
	private Vocabulary vocabulary;
	@Autowired
	private TopicMapping topicMapping;
	@Autowired
	private VectorSimilarityCalculator vectorSimilarityCalculator;
	
	//State variables
	private double[][]priors;
	
	public void init(){
		LOGGER.info("Init alpha priors (base alphaPrior={})",baseAlpha);
		priors=new double[dataset.getDocuments().size()][topicConfiguration.getTopics().size()];//new OpenIntObjectHashMap();
		for (Document document : dataset.getDocuments()) {
			processDocument(document);
		}
	}
	
	/**
	 * Calculate alpha bias for each document
	 * @param document
	 */
	private void processDocument(Document document) {
		int docId=document.getDocId();
		for(Word word:document.getWords()){
			if(word.isStopword()){
				continue;
			}
			double[]domainWordVector=domainWordVectors.getWordVector(vocabulary.getWord(word.getId()));
			double[]generalWordVector=generalWordVectors.getWordVector(vocabulary.getWord(word.getId()));
			for(Topic topic:topicConfiguration.getTopics()){
				int topicNum=topicMapping.getTopicNumber(topic.getTopicName());
				List<double[]>topicAllDomainWordVectors=Lists.newArrayList();
				topicAllDomainWordVectors.addAll(domainWordVectors.getWordVectors(topic.getAspectTerms()));
				topicAllDomainWordVectors.addAll(domainWordVectors.getWordVectors(topic.getPositives()));
				topicAllDomainWordVectors.addAll(domainWordVectors.getWordVectors(topic.getNegatives()));
				List<double[]>topicAllGeneralWordVectors=Lists.newArrayList();
				topicAllGeneralWordVectors.addAll(generalWordVectors.getWordVectors(topic.getAspectTerms()));
				topicAllGeneralWordVectors.addAll(generalWordVectors.getWordVectors(topic.getPositives()));
				topicAllGeneralWordVectors.addAll(generalWordVectors.getWordVectors(topic.getNegatives()));

				double domainSimilarity = vectorSimilarityCalculator.calculateMaxSimilarity(domainWordVector, topicAllDomainWordVectors);//*baseAlpha;
				double generalSimilarity = vectorSimilarityCalculator.calculateMaxSimilarity(generalWordVector, topicAllGeneralWordVectors);//*baseAlpha;

				priors[docId][topicNum]+= generalSimilarity*generalDomainProportion+domainSimilarity*(1-generalDomainProportion);
				
			}
		}
		priors[docId]=normalizeToZeroOne(priors[docId]);
		//normalize and multiply base prior
		for(int topic=0;topic<priors[0].length;topic++){
			priors[docId][topic]*=baseAlpha;
		}
	}

	public double getAlpha(int docId, int topic) {
		return priors[docId][topic];
	}
	
	public double getBaseAlpha(){
		return this.baseAlpha;
	}
	
	/**
	 * Only to be used for inference, i.e. dataset is the new set of unseen documents
	 * @param dataset
	 */
	public void setDatasetForInference(Dataset dataset){
		this.dataset=dataset;
	}
	
	private double[] normalizeToZeroOne(double[]values){
		double min=Double.MAX_VALUE;
		double max=Double.MIN_VALUE;
		for(int i=0;i<values.length;i++){
			min=Math.min(min, values[i]);
			max=Math.max(max, values[i]);
		}
		double[]normalizedValues=new double[values.length];
		for(int i=0;i<values.length;i++){
			normalizedValues[i]=(values[i]-min+0.01)/(max-min+0.01);//note the smoothing value to prevent problems with NaNs
		}
//		LOGGER.info("Normalizing values, original values: {}  normalized values: {}",values,normalizedValues);
		return normalizedValues;
	}
}
