package org.vicomtech.w2vlda_final.topicmodel.priors;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.datasets.state.TopicMapping;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration.Topic;
import org.vicomtech.w2vlda_final.topicmodel.estimation.Estimator;
import org.vicomtech.w2vlda_final.embeddings.WordVectors;
import org.vicomtech.w2vlda_final.embeddings.vector_utils.VectorSimilarityCalculator;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class BetaPriors {

	private static final Logger LOGGER = LoggerFactory.getLogger(BetaPriors.class);
	
	public static final int NUM_POLARITY_WORDS_FOR_PRIORS=1;
	
	@Value("${w2vlda.baseBeta}")
	private double baseBeta;
	private double baseBetaSum;

	@Autowired@Qualifier("domainWordVectors")
	private WordVectors domainWordVectors;
	@Autowired
	private TopicsConfiguration topicsConfiguration;
	@Autowired
	private VectorSimilarityCalculator vectorSimilarityCalculator;
	@Autowired
	private Vocabulary vocabulary;
	@Autowired
	private TopicMapping topicMapping;

	// State variables
	private int numTopics;

	// Stare variables, corrections
	private double[][][] betas;// indexed by <topic,wordtype,wordId>
	private double[][] betaSums;// indexed by <topic,wordtype>, summing all over the vocabulary words

	

	public void init() {
		this.numTopics = topicsConfiguration.getTopics().size();
		LOGGER.info("Init beta priors, (base betaPriors={})... (VocSize:{}, NumTopics:{})",baseBeta, vocabulary.size(), numTopics);
		this.betas = new double[numTopics][Estimator.NUM_WORD_TYPES][vocabulary.size()];
		this.betaSums = new double[numTopics][Estimator.NUM_WORD_TYPES];
		this.baseBetaSum=(double)vocabulary.size()*baseBeta;
		processWords();
	}

	/**
	 * Process all the vocabulary words to calculate the biased betas
	 */
	private void processWords() {
		Set<String>positivePriorWords=Sets.newHashSet(topicsConfiguration.getGeneralPositives());	
		Set<String>negativePriorWords=Sets.newHashSet(topicsConfiguration.getGeneralNegatives());
		LOGGER.info("Selected positives: {}",positivePriorWords);
		LOGGER.info("Selected negatives: {}",negativePriorWords);
		
		List<double[]> positiveVectors = domainWordVectors.getWordVectors(Lists.newArrayList(positivePriorWords));
		List<double[]> negativeVectors = domainWordVectors.getWordVectors(Lists.newArrayList(negativePriorWords));
		
		for (Topic topic:topicsConfiguration.getTopics()) {
			
			int topicNum = topicMapping.getTopicNumber(topic.getTopicName());
			
			List<double[]> aspectTermVectors = domainWordVectors.getWordVectors(topic.getAspectTerms());
			
			for (int wordId = 0; wordId < vocabulary.size(); wordId++) {
				double[] domainWordVector = domainWordVectors.getWordVector(vocabulary.getWord(wordId));
				
				double aspectTermTopicSim = vectorSimilarityCalculator.calculateMaxSimilarity(domainWordVector,aspectTermVectors);
				
				double positivesSim=vectorSimilarityCalculator.calculateMaxSimilarity(domainWordVector, positiveVectors);
				double negativesSim=vectorSimilarityCalculator.calculateMaxSimilarity(domainWordVector, negativeVectors);


				//set the values for each topic, word type, word
				betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES][wordId]=aspectTermTopicSim;
				betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES][wordId]=positivesSim-negativesSim;
				betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES][wordId]=negativesSim-positivesSim;

			}
			
			betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES]=normalizeToZeroOne(betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES]);
			betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES]=normalizeToZeroOne(betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES]);
			betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES]=normalizeToZeroOne(betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES]);
			
			//for the value obtained for each word, multiply for the configured baseBeta
			for (int wordId = 0; wordId < vocabulary.size(); wordId++) {
				betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES][wordId]*=baseBeta;
				betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES][wordId]*=baseBeta;
				betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES][wordId]*=baseBeta;
			}
			
			
			//sum up all the values
			for (int wordId = 0; wordId < vocabulary.size(); wordId++) {
				betaSums[topicNum][Estimator.AT_INDEX_IN_WORDTYPES]+=betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES][wordId];
				//this is correct, once true pos/neg betas get computed, this summation is valid
				betaSums[topicNum][Estimator.POS_INDEX_IN_WORDTYPES]+=betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES][wordId];
				betaSums[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES]+=betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES][wordId];
			}
		}
		
	}

	public double getBeta(int topic, int wordtype, int wordId) {
		return betas[topic][wordtype][wordId];
	}

	public double getBetaSum(int topic,int wordtype) {
		return betaSums[topic][wordtype];
	}
	
	public double getBaseBeta(){
		return baseBeta;
	}
	
	public double getBaseBetaSum(){
		return baseBetaSum;
	}
	
	private double[] normalizeToZeroOne(double[]values){
		double min=Double.MAX_VALUE;
		double max=Double.MIN_VALUE;
		for(int i=0;i<values.length;i++){
			min=Math.min(min, values[i]);
			max=Math.max(max, values[i]);
		}
		double[]normalizedValues=new double[values.length];
		for(int i=0;i<values.length;i++){
			normalizedValues[i]=(values[i]-min+0.01)/(max-min+0.01);//note the smoothing value to prevent problems with NaNs
		}
//		LOGGER.info("Normalizing values, original values: {}  normalized values: {}",values,normalizedValues);
		return normalizedValues;
	}

}
