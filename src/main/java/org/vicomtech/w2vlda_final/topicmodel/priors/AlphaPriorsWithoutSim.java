package org.vicomtech.w2vlda_final.topicmodel.priors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;


public class AlphaPriorsWithoutSim extends AlphaPriors{

	private static final Logger LOGGER=LoggerFactory.getLogger(AlphaPriorsWithoutSim.class);
	
	@Value("${w2vlda.baseAlpha}")
	private double baseAlpha;
	@Value("${w2vlda.generalDomainProportion:0.5}")
	private double generalDomainProportion;
	
	@Autowired
	private Dataset dataset;
	@Autowired
	private TopicsConfiguration topicConfiguration;
	
	//State variables
//	private Map<Integer, double[]> priors;
//	private OpenIntObjectHashMap priors;
	private double[][]priors;
	
	public void init(){
		LOGGER.info("Init alpha priors (base alphaPrior={})",baseAlpha);
		priors=new double[dataset.getDocuments().size()][topicConfiguration.getTopics().size()];//new OpenIntObjectHashMap();
		for (Document document : dataset.getDocuments()) {
			processDocument(document);
		}
	}
	
	private void processDocument(Document document) {
		int docId=document.getDocId();

		//just base prior, no info from word embedding
		for(int topic=0;topic<priors[0].length;topic++){
			priors[docId][topic]=baseAlpha;
		}
	}

	public double getAlpha(int docId, int topic) {
		return priors[docId][topic];//((double[])priors.get(docId))[topic];
	}
	
	public double getBaseAlpha(){
		return this.baseAlpha;
	}
	
	/**
	 * Only to be used for inference, i.e. dataset is the new set of unseen documents
	 * @param dataset
	 */
	public void setDatasetForInference(Dataset dataset){
		this.dataset=dataset;
	}
	
}
