package org.vicomtech.w2vlda_final.topicmodel.priors;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.datasets.state.TopicMapping;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.embeddings.WordVectors;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration.Topic;
import org.vicomtech.w2vlda_final.topicmodel.estimation.Estimator;

import com.google.common.collect.Sets;

public class BetaPriorsWithoutSim extends BetaPriors{

	private static final Logger LOGGER = LoggerFactory.getLogger(BetaPriorsWithoutSim.class);
	
	private static final int BIG_NUMBER_TO_BIAS_PRIORS=1000;
	
	//public static final double PRIOR_MULTIPLIER=2;
	public static final int NUM_POLARITY_WORDS_FOR_PRIORS=1;
	
	@Value("${w2vlda.baseBeta}")
	private double baseBeta;
	private double baseBetaSum;
	
//	@Value("${w2vlda.generalDomainProportion:0.5}")
//	private double generalDomainProportion;

	@Autowired@Qualifier("domainWordVectors")
	private WordVectors domainWordVectors;
//	@Autowired @Qualifier("generalWordVectors")
//	private WordVectors generalWordVectors;
	@Autowired
	private TopicsConfiguration topicsConfiguration;
	@Autowired
	private Vocabulary vocabulary;
	@Autowired
	private TopicMapping topicMapping;

	// State variables
	private int numTopics;

	// Stare variables, corrections
	private double[][][] betas;// indexed by <topic,wordtype,wordId>
	private double[][] betaSums;// indexed by <topic,wordtype>, summing all over the vocabulary words

	

	public void init() {
		this.numTopics = topicsConfiguration.getTopics().size();
		LOGGER.info("Init beta priors, (base betaPriors={})... (VocSize:{}, NumTopics:{})",baseBeta, vocabulary.size(), numTopics);
		this.betas = new double[numTopics][Estimator.NUM_WORD_TYPES][vocabulary.size()];
		this.betaSums = new double[numTopics][Estimator.NUM_WORD_TYPES];
		this.baseBetaSum=(double)vocabulary.size()*baseBeta;
		processWords();
	}

	private void processWords() {
		Set<String>positivePriorWords=Sets.newHashSet(topicsConfiguration.getGeneralPositives());		
		Set<String>negativePriorWords=Sets.newHashSet(topicsConfiguration.getGeneralNegatives());
		LOGGER.info("Selected positives: {}",positivePriorWords);
		LOGGER.info("Selected negatives: {}",negativePriorWords);
		
		for (Topic topic:topicsConfiguration.getTopics()) {
			
			int topicNum = topicMapping.getTopicNumber(topic.getTopicName());
			
			for (int wordId = 0; wordId < vocabulary.size(); wordId++) {
				String word=vocabulary.getWord(wordId);
				if(topic.getAspectTerms().contains(word)){
					betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES][wordId]=BIG_NUMBER_TO_BIAS_PRIORS;
				}
				if(positivePriorWords.contains(word)){
					betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES][wordId]=BIG_NUMBER_TO_BIAS_PRIORS;
				}
				if(negativePriorWords.contains(word)){
					betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES][wordId]=BIG_NUMBER_TO_BIAS_PRIORS;
				}
				
			}
			
//			betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES]=normalizeToZeroOne(betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES]);
//			betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES]=normalizeToZeroOne(betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES]);
//			betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES]=normalizeToZeroOne(betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES]);
			
			for (int wordId = 0; wordId < vocabulary.size(); wordId++) {
				betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES][wordId]+=baseBeta;
				betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES][wordId]+=baseBeta;
				betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES][wordId]+=baseBeta;
			}
			
			for (int wordId = 0; wordId < vocabulary.size(); wordId++) {
				betaSums[topicNum][Estimator.AT_INDEX_IN_WORDTYPES]+=betas[topicNum][Estimator.AT_INDEX_IN_WORDTYPES][wordId];
				//this is correct, once true pos/neg betas get computed, this summation is valid
				betaSums[topicNum][Estimator.POS_INDEX_IN_WORDTYPES]+=betas[topicNum][Estimator.POS_INDEX_IN_WORDTYPES][wordId];
				betaSums[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES]+=betas[topicNum][Estimator.NEG_INDEX_IN_WORDTYPES][wordId];
			}
		}
		
	}

	public double getBeta(int topic, int wordtype, int wordId) {
		return betas[topic][wordtype][wordId];
	}

	public double getBetaSum(int topic,int wordtype) {
		return betaSums[topic][wordtype];
	}
	
	public double getBaseBeta(){
		return baseBeta;
	}
	
	public double getBaseBetaSum(){
		return baseBetaSum;
	}

}
