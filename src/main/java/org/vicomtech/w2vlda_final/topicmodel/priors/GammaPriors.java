package org.vicomtech.w2vlda_final.topicmodel.priors;

import java.util.List;

//import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.datasets.state.Word;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.estimation.Estimator;
import org.vicomtech.w2vlda_final.embeddings.WordVectors;
import org.vicomtech.w2vlda_final.embeddings.vector_utils.VectorSimilarityCalculator;

public class GammaPriors {

	private static final Logger LOGGER=LoggerFactory.getLogger(GammaPriors.class);
	
	
	@Value("${w2vlda.baseGamma}")
	private double baseGamma;
	
	@Autowired
	private Dataset dataset;
	@Autowired
	private TopicsConfiguration topicConfiguration;
	@Autowired @Qualifier("domainWordVectors")
	private WordVectors domainWordVectors;
	@Autowired
	private Vocabulary vocabulary; 
	@Autowired
	private VectorSimilarityCalculator vectorSimilarityCalculator;
	
	//State variables
	private double[][]gammas;
	
	public void init(){
		LOGGER.info("Init gamma priors (base gammaPrior={})",baseGamma);
		gammas=new double[dataset.getDocuments().size()][topicConfiguration.getTopics().size()];//Maps.newHashMap();
		for (Document document : dataset.getDocuments()) {
			processDocument(document);
		}
	}
	
	/**
	 * Process document to calculate the biased gammas
	 * @param document
	 */
	private void processDocument(Document document) {
		int docId=document.getDocId();
		for(Word word:document.getWords()){
			if(word.isStopword()){
				continue;
			}
			double[]domainWordVector=domainWordVectors.getWordVector(vocabulary.getWord(word.getId()));
			
			List<double[]>domainPositiveVectors=domainWordVectors.getWordVectors(topicConfiguration.getGeneralPositives());
			List<double[]>domainNegativeVectors=domainWordVectors.getWordVectors(topicConfiguration.getGeneralNegatives());
			double positiveSim=vectorSimilarityCalculator.calculateMaxSimilarity(domainWordVector, domainPositiveVectors);
			double negativeSim=vectorSimilarityCalculator.calculateMaxSimilarity(domainWordVector, domainNegativeVectors);
			gammas[docId][Estimator.POSITIVE_INDEX_IN_DOCPOLARITY]+=positiveSim-negativeSim;
			gammas[docId][Estimator.NEGATIVE_INDEX_IN_DOCPOLARITY]+=negativeSim-positiveSim;
			
		}
		
		gammas[docId]=normalizeToZeroOne(gammas[docId]);
		
		//normalize and multiply base prior
		for(int i=0;i<Estimator.NUM_POLARITY_TYPES;i++){
			gammas[docId][i]*=baseGamma;
		}
	}

	public double getGamma(int docId, int polarity) {
		return gammas[docId][polarity];//priors.get(docId)[polarity];
	}
	
	public double getBaseGamma(){
		return this.baseGamma;
	}
	
	private double[] normalizeToZeroOne(double[]values){
		double min=Double.MAX_VALUE;
		double max=Double.MIN_VALUE;
		for(int i=0;i<values.length;i++){
			min=Math.min(min, values[i]);
			max=Math.max(max, values[i]);
		}
		double[]normalizedValues=new double[values.length];
		for(int i=0;i<values.length;i++){
			normalizedValues[i]=(values[i]-min+0.01)/(max-min+0.01);//note the smoothing value to prevent problems with NaNs
		}
//		LOGGER.info("Normalizing values, original values: {}  normalized values: {}",values,normalizedValues);
		return normalizedValues;
	}
	
}
