package org.vicomtech.w2vlda_final.topicmodel.estimation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.vicomtech.w2vlda_final.clusters.WordClusters;
import org.vicomtech.w2vlda_final.datasets.io.DatasetReader;
import org.vicomtech.w2vlda_final.datasets.io.StopwordsHandler;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.TopicMapping;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.maxent.MaxEntClassifier;
import org.vicomtech.w2vlda_final.maxent.MaxEntTrainingSetGenerator;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfigurationIO;
import org.vicomtech.w2vlda_final.topicmodel.priors.AlphaPriors;
import org.vicomtech.w2vlda_final.topicmodel.priors.AlphaPriorsWithoutSim;
import org.vicomtech.w2vlda_final.topicmodel.priors.BetaPriors;
import org.vicomtech.w2vlda_final.topicmodel.priors.BetaPriorsWithoutSim;
import org.vicomtech.w2vlda_final.topicmodel.priors.GammaPriors;
import org.vicomtech.w2vlda_final.topicmodel.priors.GammaPriorsWithoutSim;
import org.vicomtech.w2vlda_final.embeddings.WordVectors;
import org.vicomtech.w2vlda_final.embeddings.vector_utils.VectorSimilarityCalculator;

/**
 * Spring configuration bean for the dependency injection (this is just technical software stuff)
 * Here is where the beans are created previous to auto-wiring them along the whole application
 * @author yo
 *
 */
@Configuration
public class BaseSpringConfiguration {
	
	private static final boolean USE_WORD_EMBEDDINGS_FOR_PRIORS=true;
	
	@Bean
	public DatasetReader datasetReader(){
		return new DatasetReader();
	}
	
	@Bean
	public Dataset dataset(@Value("${w2vlda.dataset}")String datasetPath){
		return datasetReader().readDataset(datasetPath);//I'm not sure if this is going to work
	}
	
	@Bean(initMethod="init")
	public StopwordsHandler stopwordsHandler(){
		return new StopwordsHandler();
	}
	
	@Bean(name="brownClusters")
	public WordClusters brownClusters(@Value("${w2vlda.brown_clusters}")String pathToClustersFile){
		return WordClusters.loadBrownClusters(pathToClustersFile);
	}
	
	@Bean(initMethod="init")
	public MaxEntClassifier maxEntClassifier(){
		return new MaxEntClassifier();
	}
	
	@Bean
	public MaxEntTrainingSetGenerator maxEntTrainingSetGenerator(){
		return new MaxEntTrainingSetGenerator();
	}
	
	@Bean
	public Vocabulary vocabulary(){
		return new Vocabulary();
	}
	
	@Bean
	public TopicMapping topicMapping(){
		return new TopicMapping();
	}
	
	@Bean
	public TopicsConfiguration topicsConfiguration(@Value("${w2vlda.topics}")String topicsConfigFile) throws FileNotFoundException{
		TopicsConfigurationIO topicsConfigurationIO=new TopicsConfigurationIO();
		return topicsConfigurationIO.readTopicsConfiguration(new FileInputStream(topicsConfigFile));
	}
	
	@Bean(initMethod="init")
	@DependsOn("dataset")
	public AlphaPriors alphaPriors(){
		if(USE_WORD_EMBEDDINGS_FOR_PRIORS){
			return new AlphaPriors();
		}else{
			return new AlphaPriorsWithoutSim();
		}		
	}
	
	@Bean(initMethod="init")
	@DependsOn("dataset")
	public BetaPriors betaPriors(){
		if(USE_WORD_EMBEDDINGS_FOR_PRIORS){
			return new BetaPriors();
		}else{
			return new BetaPriorsWithoutSim();
		}
	}
	
	@Bean(initMethod="init")
	@DependsOn("dataset")
	public GammaPriors gammaPriors(){
		if(USE_WORD_EMBEDDINGS_FOR_PRIORS){
			return new GammaPriors();
		}else{
			return new GammaPriorsWithoutSim();
		}
	}
	
	@Bean
	public Random random(@Value("${w2vlda.random_seed}")String randomSeedProp){
		return new Random(Long.parseLong(randomSeedProp));
	}
	
	@Bean(initMethod="init")
	public VectorSimilarityCalculator vectorSimilarityCalculator(){
		return new VectorSimilarityCalculator();
	}
	
	@Bean
	public WordVectors domainWordVectors(@Value("${w2vlda.domain_vectors}")String vectorsPath){
		return new WordVectors(vectorsPath);
	}
	
	@Bean
	public WordVectors generalWordVectors(@Value("${w2vlda.general_vectors}")String vectorsPath){
		return new WordVectors(vectorsPath);
	}
	
	@Bean(initMethod="init")
	@DependsOn("dataset")
	public Estimator estimator(){
		return new Estimator();
	}
	
}
