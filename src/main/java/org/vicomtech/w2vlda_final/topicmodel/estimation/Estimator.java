package org.vicomtech.w2vlda_final.topicmodel.estimation;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.clusters.WordClusters;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.datasets.state.Word;
import org.vicomtech.w2vlda_final.maxent.MaxEntClassifier;
import org.vicomtech.w2vlda_final.maxent.MaxEntTrainingSetGenerator;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.priors.AlphaPriors;
import org.vicomtech.w2vlda_final.topicmodel.priors.BetaPriors;
import org.vicomtech.w2vlda_final.topicmodel.priors.GammaPriors;
import org.vicomtech.w2vlda_final.utils.IntegerCategoricalSampler;

import com.google.common.collect.Lists;

/**
 * The class that performs the estimations.
 * The biased Dirichlet hyperparameters (alphas, betas, gammas) come already computed and cached.
 * The process iterates N times over the full datasets, performing Gibbs sampling and updating counts for topics, word types and polarities
 * @author yo
 *
 */
public class Estimator {

	private static final Logger LOGGER = LoggerFactory.getLogger(Estimator.class);


	private static final int UNNASIGNED_VALUE = -1;
	///////////////////////////////////////////
	// This is hard-coded here, dirty but simple
	// private static final int BG_INDEX=0;
	public static final int AT_INDEX_IN_WORDTYPES = 0;
	public static final int POS_INDEX_IN_WORDTYPES = 1;
	public static final int NEG_INDEX_IN_WORDTYPES = 2;
	public static final int OW_INDEX_IN_WORDTYPES=3;//CAREFUL, NOT REALLY AND INDEX! ITS A LABEL!
	public static final int NUM_WORD_TYPES = 3;
	/////
	//Aux var to indicate the index in maxent classification outcomes
	private static final int AT_INDEX_IN_MAXENT_OUTCOMES=0;
	private static final int OW_INDEX_IN_MAXENT_OUTCOMES=1;
	///////////////////////////////////////////
	public static final int POSITIVE_INDEX_IN_DOCPOLARITY = 0;
	public static final int NEGATIVE_INDEX_IN_DOCPOLARITY = 1;
	public static final int NUM_POLARITY_TYPES = 2;
	//////
	private static final Object ASPECT_TERM_LABEL = "ASPECT_TERM";
	private static final Object OPINION_WORD_LABEL = "OPINION_WORD";

	@Autowired
	private Dataset dataset;
	@Autowired
	private TopicsConfiguration topicsConfiguration;
	@Autowired
	protected Random random;
	@Autowired
	private Vocabulary vocabulary;
	@Autowired
	private AlphaPriors alphaPriors;
	@Autowired
	private BetaPriors betaPriors;
	@Autowired
	private GammaPriors gammaPriors;
	@Autowired
	private MaxEntClassifier maxEntClassifier;
	@Autowired
	private MaxEntTrainingSetGenerator maxEntTrainingSetGenerator;
	@Autowired
	@Qualifier("brownClusters")
	private WordClusters brownClusters;

	////
	private List<WordClusters> wordClustersToGenerateFeatures;

	@Value("${w2vlda.iterations}")
	private int numIterations;
	@Value("${w2vlda.burnin_iterations}")
	private int burninIterations;
	@Value("${w2vlda.sampling_lag}")
	private int samplingLag;

	// STATE VARIABLES
	private double[][] documentTopicsCount;// indexed by <doc,topic>
	private double[][][] topicWordtypeWordCount;// indexed by
												// <topic,wordtype,word>
	private double[][] topicWordtypeWordSum;// indexed by <topic,wordtype>

	private double[][] documentPolarityCount;// indexed by <doc,polarity>

	// helper vars
	protected int numTopics;
	protected int numDocuments;
	protected int numWords;

	// private int backgroundTopicIndex;

	// Accum vars
	private double[][] theta;// the document-topic accum
	private double[][][] phi;// the topic-wordtype-word accum
	// private double[][][] phiPolarity; //the topic-polarity-word accum
	private double[][] omega;// the document-polarity accum
	private double numAccumulations;

	// Categorical sampler (note that sharing a categorical sampler makes the
	// code thread-unsafe)
	private IntegerCategoricalSampler categoricalSampler;

	public void init() {
		LOGGER.info("Initializing estimator (also with polarities)...");

		categoricalSampler = new IntegerCategoricalSampler(random);

		this.numTopics = topicsConfiguration.getTopics().size();
		this.numDocuments = (int) dataset.size();
		this.numWords = vocabulary.size();
		randomInitModelVariablesAndState();
		initAccumulationSamples();
		wordClustersToGenerateFeatures = Lists.newArrayList();
		wordClustersToGenerateFeatures.add(brownClusters);
	}

	private void initAccumulationSamples() {
		theta = new double[numDocuments][numTopics];
		phi = new double[numTopics][NUM_WORD_TYPES][numWords];
		omega = new double[numDocuments][NUM_POLARITY_TYPES];
		numAccumulations = 0;
	}

	/**
	 * Random initialization of the model state, the initial state for the Gibbs sampling process
	 */
	private void randomInitModelVariablesAndState() {
		LOGGER.info("Random initialization of model variables and topic/sentiment assignments...");
		documentTopicsCount = new double[numDocuments][numTopics];
		topicWordtypeWordCount = new double[numTopics][NUM_WORD_TYPES][numWords];
		topicWordtypeWordSum = new double[numTopics][NUM_WORD_TYPES];
		documentPolarityCount = new double[numDocuments][NUM_POLARITY_TYPES];
		for (Document document : dataset.getDocuments()) {
			for (Word word : document.getWords()) {
				if (word.isStopword()) {
					continue;
				}
				int randomTopic = random.nextInt(numTopics);
				int randomWordType = random.nextInt(NUM_WORD_TYPES);
				word.setTopic(randomTopic);
				word.setWordType(randomWordType);
				documentTopicsCount[document.getDocId()][randomTopic]++;
				topicWordtypeWordCount[randomTopic][randomWordType][word.getId()]++;
				topicWordtypeWordSum[randomTopic][randomWordType]++;
				// if the random word type was OW, sample also a polarity
				if (randomWordType == POS_INDEX_IN_WORDTYPES) {
					documentPolarityCount[document.getDocId()][POSITIVE_INDEX_IN_DOCPOLARITY]++;
				} else if (randomWordType == NEG_INDEX_IN_WORDTYPES) {
					documentPolarityCount[document.getDocId()][NEGATIVE_INDEX_IN_DOCPOLARITY]++;
				}

			}
		}
	}

	/**
	 * Estimate the model performing Gibbs sampling of the topic, word-type and polarity, iterating many times over the full dataset
	 */
	public void estimate() {
		///////////////////
		int topic=0;//topic does not matter, all the same beta polarities
		int at=0;
		int posit=1;
		int neg=2;
		List<String>linesToPrintAT=Lists.newArrayList();
		for(int wordId=0;wordId<vocabulary.size();wordId++){
			StringBuffer sb=new StringBuffer(vocabulary.getWord(wordId)+" --> betaAT: "+betaPriors.getBeta(topic, at, wordId));
			sb.append("\t\t");
			sb.append(vocabulary.getWord(wordId)+" --> betaPOS: "+betaPriors.getBeta(topic, posit, wordId));
			sb.append("\t\t");
			sb.append(vocabulary.getWord(wordId)+" --> betaNEG: "+betaPriors.getBeta(topic, neg, wordId));
			linesToPrintAT.add(sb.toString());
		}
		
		///////////////////
		LOGGER.info("Vocabulary size at the beginning of the model estimation: {} words", vocabulary.size());
		LOGGER.info("Starting model estimation to perform {} iterations", numIterations);
		for (int iter = 1; iter <= numIterations; iter++) {
			long time = System.currentTimeMillis();
			doIteration();
			if(iter%5==0){
				LOGGER.info("Iteration {} of {}, numDocs: {} (duration: {} milis)", iter, numIterations,dataset.size(),
					System.currentTimeMillis() - time);
			}
			
			if (iter < burninIterations) {
				LOGGER.info("Burn-in iteration (configured to first {} iterations)", burninIterations);
			} else if (iter > 0 && iter % samplingLag == 0) {
				LOGGER.info("Taking snapshot (every {} iterations)", samplingLag);
				accumSample();
			}
		}
		if (numAccumulations > 0) {
			divideAccumByNumSamples();
		}

	}

	private void divideAccumByNumSamples() {
		for (int docId = 0; docId < numDocuments; docId++) {
			for (int topic = 0; topic < numTopics; topic++) {
				theta[docId][topic] /= numAccumulations;
			}
			for (int polarity = 0; polarity < NUM_POLARITY_TYPES; polarity++) {
				omega[docId][polarity] /= numAccumulations;
			}
		}
		for (int topic = 0; topic < numTopics; topic++) {
			for (int wordType = 0; wordType < NUM_WORD_TYPES; wordType++) {
				for (int wordId = 0; wordId < numWords; wordId++) {
					phi[topic][wordType][wordId] /= numAccumulations;
				}
			}
		}
	}

	private void accumSample() {
		for (int docId = 0; docId < numDocuments; docId++) {
			for (int topic = 0; topic < numTopics; topic++) {
				theta[docId][topic] += documentTopicsCount[docId][topic];
			}
			for (int polarity = 0; polarity < NUM_POLARITY_TYPES; polarity++) {
				omega[docId][polarity] += documentPolarityCount[docId][polarity];
			}
		}
		for (int topic = 0; topic < numTopics; topic++) {
			for (int wordType = 0; wordType < NUM_WORD_TYPES; wordType++) {
				for (int wordId = 0; wordId < numWords; wordId++) {
					phi[topic][wordType][wordId] += topicWordtypeWordCount[topic][wordType][wordId];
				}
			}
		}
		numAccumulations++;
		LOGGER.info("Increased num accumulations to {}", numAccumulations);
	}

	/**
	 * A single iteration
	 */
	private void doIteration() {
		//LOGGER.info("Iterating over {} documents", (int) dataset.size());
		List<Document>documents=dataset.getDocuments();
		//LOGGER.info("Shuffling document order");
		Collections.shuffle(documents);
		for (Document doc : documents){//dataset.getDocuments()) {
			 //LOGGER.info("Current doc: {}", doc.toString());
			for (int wordIndex = 0; wordIndex < doc.getWords().size(); wordIndex++) {
				Word word = doc.getWords().get(wordIndex);
				if (word.isStopword()) {
					continue;
				}
				removeTopicAssignment(word);
				int topic = sampleTopic(word);
				assignTopic(word, topic);
				int wordType = sampleWordTypeAndPolarity(word, wordIndex);
				assignWordType(word, wordType);
			}
		}
	}

	/**
	 * Topic sampling from the expected probability distribution (Gibbs sampling)
	 * @param word
	 * @return
	 */
	protected int sampleTopic(Word word) {
		int docId = word.getParent().getDocId();
		int wordId = word.getId();
		//int wordType = word.getWordType();
		categoricalSampler.clearSamplerValues();
		for (int topic = 0; topic < numTopics; topic++) {
			double ndt = documentTopicsCount[docId][topic];
			double alphadt = alphaPriors.getAlpha(docId, topic);
			///
			double prob = (ndt + alphadt);
			for (int wordtype = 0; wordtype < NUM_WORD_TYPES; wordtype++) {
				double ntw = topicWordtypeWordCount[topic][wordtype][wordId];
				double betatw = betaPriors.getBeta(topic,wordtype, wordId);
				double nTW = topicWordtypeWordSum[topic][wordtype];
				double betaTW = betaPriors.getBetaSum(topic,wordtype);
				////
				double thisWordTypeComponent = (ntw + betatw) / (nTW + betaTW);
				prob *= thisWordTypeComponent;
				//LOGGER.info("Word ({}) counts, ntw:{} betatw:{} nTW:{} betaTW:{} ndt:{} alphadt:{}",word.getWordform(),ntw,betatw,nTW,betaTW,ndt,alphadt);
			}
			//LOGGER.info("Adding topic sampling value for word {}, topic {}, prob {}",word.getWordform(),topic,prob);
			categoricalSampler.addItem(topic, prob);
		}
		return categoricalSampler.sample();
	}

	/**
	 * Wordtype and polarity sampling from the expected probability distributions (Gibbs sampling)
	 * @param word
	 * @param wordIndex
	 * @return
	 */
	private int sampleWordTypeAndPolarity(Word word, int wordIndex) {

		int wordId = word.getId();
		int topic = word.getTopic();
		Document doc = word.getParent();
		int docId=doc.getDocId();
		categoricalSampler.clearSamplerValues();
		String[] features = maxEntTrainingSetGenerator.generateFeatureString2(wordClustersToGenerateFeatures, doc,
				wordIndex);
		Map<String, Double> probs = maxEntClassifier.evaluateInstance2(features);
		double atProb = Math.exp(probs.get(ASPECT_TERM_LABEL));
		double owProb =  Math.exp(probs.get(OPINION_WORD_LABEL));
		double sum = atProb + owProb;
		double[] atowProbs = new double[] { atProb / sum, owProb / sum };
		
		double ntwAT=topicWordtypeWordCount[topic][AT_INDEX_IN_WORDTYPES][wordId];
		double ntwOW=topicWordtypeWordCount[topic][POS_INDEX_IN_WORDTYPES][wordId]+topicWordtypeWordCount[topic][NEG_INDEX_IN_WORDTYPES][wordId];
		double betaBase=betaPriors.getBaseBeta();
		double nTWAT=topicWordtypeWordSum[topic][AT_INDEX_IN_WORDTYPES];
		double nTWOW=topicWordtypeWordSum[topic][POS_INDEX_IN_WORDTYPES]+topicWordtypeWordSum[topic][NEG_INDEX_IN_WORDTYPES];
		double betaBaseSum=betaPriors.getBaseBetaSum();
		
		double probAT=((ntwAT+betaBase)/(nTWAT+betaBaseSum))*atowProbs[AT_INDEX_IN_MAXENT_OUTCOMES];
		double probOW=((ntwOW+betaBase)/(nTWOW+betaBaseSum))*atowProbs[OW_INDEX_IN_MAXENT_OUTCOMES];
		////////////////DISABLED THE PROBABILITY FOR THE EXPERIMENT (don't touch this)
		boolean disableATOWProbability=false;
		if(disableATOWProbability){
			//equal prob to randomize the separation
			categoricalSampler.addItem(AT_INDEX_IN_WORDTYPES, 0.5);
			categoricalSampler.addItem(OW_INDEX_IN_WORDTYPES, 0.5);
		}else{
			categoricalSampler.addItem(AT_INDEX_IN_WORDTYPES, probAT);
			categoricalSampler.addItem(OW_INDEX_IN_WORDTYPES, probOW);
		}
		/////////////////
		int sampledWordType=categoricalSampler.sample();
		if(sampledWordType==AT_INDEX_IN_WORDTYPES){
			//it the sampling says "aspect term", the we are done
			return sampledWordType;
		}else{
			//if the sampling says "opinion word", then we sample the polarity (positive or negative)
			///POS
			double ntwPOS=topicWordtypeWordCount[topic][POS_INDEX_IN_WORDTYPES][wordId];
			double betatwPOS = betaPriors.getBeta(topic,POS_INDEX_IN_WORDTYPES, wordId);
			double nTWPOS = topicWordtypeWordSum[topic][POS_INDEX_IN_WORDTYPES];
			double betaTWPOS = betaPriors.getBetaSum(topic,POS_INDEX_IN_WORDTYPES);
			///NEG
			double ntwNEG=topicWordtypeWordCount[topic][NEG_INDEX_IN_WORDTYPES][wordId];
			double betatwNEG = betaPriors.getBeta(topic,NEG_INDEX_IN_WORDTYPES, wordId);
			double nTWNEG = topicWordtypeWordSum[topic][NEG_INDEX_IN_WORDTYPES];
			double betaTWNEG = betaPriors.getBetaSum(topic,NEG_INDEX_IN_WORDTYPES);
			///
			double totalPolarityWordsInThisDoc=documentPolarityCount[doc.getDocId()][POSITIVE_INDEX_IN_DOCPOLARITY]+documentPolarityCount[doc.getDocId()][NEGATIVE_INDEX_IN_DOCPOLARITY];
			double probDocumentPOS=(documentPolarityCount[doc.getDocId()][POSITIVE_INDEX_IN_DOCPOLARITY]+gammaPriors.getGamma(docId, POSITIVE_INDEX_IN_DOCPOLARITY))/(totalPolarityWordsInThisDoc+2*gammaPriors.getGamma(docId, POSITIVE_INDEX_IN_DOCPOLARITY));
			double probDocumentNEG=(documentPolarityCount[doc.getDocId()][NEGATIVE_INDEX_IN_DOCPOLARITY]+gammaPriors.getGamma(docId, NEGATIVE_INDEX_IN_DOCPOLARITY))/(totalPolarityWordsInThisDoc+2*gammaPriors.getGamma(docId, NEGATIVE_INDEX_IN_DOCPOLARITY));
			
			double totalProbPOS=((ntwPOS+betatwPOS)/(nTWPOS+betaTWPOS))*probDocumentPOS;
			double totalProbNEG=((ntwNEG+betatwNEG)/(nTWNEG+betaTWNEG))*probDocumentNEG;
			
			categoricalSampler.clearSamplerValues();
			categoricalSampler.addItem(POS_INDEX_IN_WORDTYPES, totalProbPOS);
			categoricalSampler.addItem(NEG_INDEX_IN_WORDTYPES, totalProbNEG);
			return categoricalSampler.sample();
		}
		
	}

	/**
	 * Removes all the assignments and its associated counts (topic, wordtype,
	 * polarity)
	 * 
	 * @param word
	 */
	private void removeTopicAssignment(Word word) {
		int wordId = word.getId();
		int docId = word.getParent().getDocId();
		int topic = word.getTopic();
		int wordType = word.getWordType();
		try {
			documentTopicsCount[docId][topic]--;
			topicWordtypeWordCount[topic][wordType][wordId]--;
			topicWordtypeWordSum[topic][wordType]--;
			if (wordType == POS_INDEX_IN_WORDTYPES) {
				documentPolarityCount[docId][POSITIVE_INDEX_IN_DOCPOLARITY]--;
			} else if (wordType == NEG_INDEX_IN_WORDTYPES) {
				documentPolarityCount[docId][NEGATIVE_INDEX_IN_DOCPOLARITY]--;
			}
			word.setTopic(UNNASIGNED_VALUE);
			word.setWordType(UNNASIGNED_VALUE);
		} catch (Exception e) {
			LOGGER.error("Some error, wordId:{} docID:{} topic:{} wordType:{}", wordId, docId, topic, wordType);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Assigns topic updating corresponding counts, the rest of the variables
	 * remain unassigned
	 * 
	 * @param word
	 * @param topic
	 */
	private void assignTopic(Word word, int topic) {
		//LOGGER.info("Assigning topic {} to word with id {} at document {}",topic,word.getId(),word.getParent().getDocId());
		word.setTopic(topic);
		documentTopicsCount[word.getParent().getDocId()][topic]++;
	}

	/**
	 * Assigns the wordtype, updating corresponding counts (included the
	 * doc-polarity in case of pos/neg)
	 * 
	 * @param word
	 * @param wordType
	 */
	private void assignWordType(Word word, int wordType) {
		topicWordtypeWordCount[word.getTopic()][wordType][word.getId()]++;
		topicWordtypeWordSum[word.getTopic()][wordType]++;
		if (wordType == POS_INDEX_IN_WORDTYPES) {
			documentPolarityCount[word.getParent().getDocId()][POSITIVE_INDEX_IN_DOCPOLARITY]++;
		} else if (wordType == NEG_INDEX_IN_WORDTYPES) {
			documentPolarityCount[word.getParent().getDocId()][NEGATIVE_INDEX_IN_DOCPOLARITY]++;
		}
		word.setWordType(wordType);
	}

	public double[][][] getPhi() {
		return phi;
	}

	public double[][] getTheta() {
		return theta;
	}

	public double[][] getOmega() {
		return omega;
	}

	public double[][] getDocumentTopicsCount() {
		return documentTopicsCount;
	}

	public double[][][] getTopicWordtypeWordCount() {
		return topicWordtypeWordCount;
	}

	public double[][] getTopicWordtypeWordSum() {
		return topicWordtypeWordSum;
	}

}
