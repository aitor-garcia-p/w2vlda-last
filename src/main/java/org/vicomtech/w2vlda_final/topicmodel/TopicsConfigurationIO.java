package org.vicomtech.w2vlda_final.topicmodel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration.Topic;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;

public class TopicsConfigurationIO {

	public TopicsConfiguration readTopicsConfiguration(InputStream is) {
		try {
			YamlReader reader = new YamlReader(new InputStreamReader(is));
			reader.getConfig().setPropertyElementType(TopicsConfiguration.class, "topics", Topic.class);
			TopicsConfiguration topicsConfiguration = reader.read(TopicsConfiguration.class);
			return topicsConfiguration;
		} catch (IOException e) {
			throw new RuntimeException("Error loading topics configuration", e);
		}
	}
	
	public TopicsConfiguration readTopicsConfiguration(String path) {
		try {
			return readTopicsConfiguration(new FileInputStream(path));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public void writeTopicsConfiguration(TopicsConfiguration topicsConfiguration, OutputStream os) {
		YamlWriter writer;
		try {
			writer = new YamlWriter(new OutputStreamWriter(os));
			writer.getConfig().setPropertyElementType(TopicsConfiguration.class, "topics", Topic.class);
			writer.write(topicsConfiguration);
			writer.close();
		} catch (IOException e) {
			throw new RuntimeException("Problem writing topics configuration", e);
		}
	}

}
