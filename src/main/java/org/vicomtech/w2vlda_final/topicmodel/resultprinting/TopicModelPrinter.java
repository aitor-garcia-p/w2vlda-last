package org.vicomtech.w2vlda_final.topicmodel.resultprinting;

import java.io.PrintStream;
import java.util.Collections;
import java.util.List;

import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;
import org.vicomtech.w2vlda_final.datasets.state.TopicMapping;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.utils.ValueComparablePair;

import com.google.common.collect.Lists;

public class TopicModelPrinter {

	private static final int COLUMN_WIDTH=30;
	
	private Vocabulary vocabulary;
	private TopicMapping topicMapping;
	private int numWordsPerDist;

	public TopicModelPrinter(Vocabulary vocabulary, TopicMapping topicMapping, int numWordsPerDist) {
		super();
		this.vocabulary = vocabulary;
		this.topicMapping = topicMapping;
		this.numWordsPerDist=numWordsPerDist;
	}

	public void printWordDistributions(double[][][]wordDistributions, PrintStream ps){
		for(int topic=0;topic<topicMapping.getNumTopics();topic++){
			ps.println("TOPIC: "+topicMapping.getTopicName(topic));
			ps.println(repeatSymbol('-', COLUMN_WIDTH*3));
			printTopic(wordDistributions[topic],ps);
			ps.println(repeatSymbol('=', COLUMN_WIDTH*3));
		}
		ps.flush();
	}

	private void printTopic(double[][] wordTypeDistributions, PrintStream ps) {
		List<ValueComparablePair>aspectTerms=orderValues(wordTypeDistributions[0]);
		List<ValueComparablePair>positives=orderValues(wordTypeDistributions[1]);
		List<ValueComparablePair>negatives=orderValues(wordTypeDistributions[2]);
		StringBuilder sb=new StringBuilder();
		String aspectTermsHeader="ASPECT_TERMS";
		String positivesHeader="POSITIVES";
		String negativesHeader="NEGATIVES";
		sb.append(aspectTermsHeader+whitespacePadding(COLUMN_WIDTH-aspectTermsHeader.length()));
		sb.append(positivesHeader+whitespacePadding(COLUMN_WIDTH-positivesHeader.length()));
		sb.append(negativesHeader+whitespacePadding(COLUMN_WIDTH-negativesHeader.length()));
		sb.append(System.lineSeparator());
		sb.append(repeatSymbol('-', sb.length()));
		sb.append(System.lineSeparator());
		for(int i=0;i<Math.min(numWordsPerDist,aspectTerms.size());i++){
			String aspectTerm=String.format("%s:%.1f", aspectTerms.get(i)._1,aspectTerms.get(i)._2);
			String positive=String.format("%s:%.1f", positives.get(i)._1,positives.get(i)._2);
			String negative=String.format("%s:%.1f", negatives.get(i)._1,negatives.get(i)._2);
			sb.append(aspectTerm+whitespacePadding(COLUMN_WIDTH-aspectTerm.length()));
			sb.append(positive+whitespacePadding(COLUMN_WIDTH-positive.length()));
			sb.append(negative+whitespacePadding(COLUMN_WIDTH-negative.length()));
			sb.append(System.lineSeparator());
		}
		ps.println(sb.toString());
	}
	
	public void printTopDocumentsPerTopics(Dataset dataset,double[][]theta,PrintStream ps){
		for(int topic=0;topic<topicMapping.getNumTopics();topic++){
			List<ValueComparablePair>documentTopicProbs=Lists.newArrayList();
			for(Document document:dataset.getDocuments()){
				documentTopicProbs.add(new ValueComparablePair(document.toString(), theta[document.getDocId()][topic]));
			}
			Collections.sort(documentTopicProbs);
			ps.println("TOP SENTENCES FOR TOPIC: "+topicMapping.getTopicName(topic));
			ps.println(repeatSymbol('-', COLUMN_WIDTH*3));
			for(ValueComparablePair docAndTopicProb:documentTopicProbs.subList(0, 10)){
				ps.println(docAndTopicProb._1);
			}
			ps.println(repeatSymbol('=', COLUMN_WIDTH*3));
		}
	}

	
	public void printTopDocumentsPerPolarity(Dataset dataset,double[][]omega,PrintStream ps){
		ps.println(repeatSymbol('*', COLUMN_WIDTH*3));
		ps.println("* TOP SENTENCES BY POLARITY");
		ps.println(repeatSymbol('*', COLUMN_WIDTH*3));
		for(int polarity=0;polarity<2;polarity++){
			List<ValueComparablePair>documentTopicProbs=Lists.newArrayList();
			for(Document document:dataset.getDocuments()){
				documentTopicProbs.add(new ValueComparablePair(document.toString(), omega[document.getDocId()][polarity]));
			}
			Collections.sort(documentTopicProbs);
			String polarityLabel=polarity==0?"POSITIVE":"NEGATIVE";
			ps.println("TOP SENTENCES FOR POLARITY: "+polarityLabel);
			ps.println(repeatSymbol('-', COLUMN_WIDTH*3));
			for(ValueComparablePair docAndTopicProb:documentTopicProbs.subList(0, 10)){
				ps.println(docAndTopicProb._1);
			}
			ps.println(repeatSymbol('=', COLUMN_WIDTH*3));
		}
	}
	
	private List<ValueComparablePair> orderValues(double[] wordDist) {
		List<ValueComparablePair>pairs=Lists.newArrayList();
		for(int wordId=0;wordId<wordDist.length;wordId++){
			pairs.add(new ValueComparablePair(vocabulary.getWord(wordId), wordDist[wordId]));
		}
		Collections.sort(pairs);
		return pairs;
	}

	private String whitespacePadding(int num){
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<num;i++){
			sb.append(' ');
		}
		return sb.toString();
	}
	
	private String repeatSymbol(char character,int times){
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<times;i++){
			sb.append(character);
		}
		return sb.toString();
	}
	
}
