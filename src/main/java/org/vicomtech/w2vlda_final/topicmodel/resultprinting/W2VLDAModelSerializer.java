package org.vicomtech.w2vlda_final.topicmodel.resultprinting;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang.SerializationUtils;
//import org.springframework.beans.factory.annotation.Value;
import org.vicomtech.w2vlda_final.topicmodel.W2VLDAModel;

public class W2VLDAModelSerializer {

//	@Value("${w2vlda.w2vlda_model}")
//	private String w2vldaModelPath;
	
	
	public static void serialize(W2VLDAModel model,OutputStream os){
		SerializationUtils.serialize(model,os);
	}
	
	public static W2VLDAModel deserialize(InputStream is){
		return (W2VLDAModel) SerializationUtils.deserialize(is);
	}
	
	public static void serialize(W2VLDAModel model,String outputPath){
		try {
			GZIPOutputStream gz = new GZIPOutputStream(new FileOutputStream(outputPath));
			SerializationUtils.serialize(model,gz);
		} catch (IOException e) {
			throw new RuntimeException("Problem serializing w2vlda model",e);
		}
	}
	
	public static W2VLDAModel deserialize(String inputPath){
		try {
			GZIPInputStream gz=new GZIPInputStream(new FileInputStream(inputPath));
			return (W2VLDAModel) SerializationUtils.deserialize(gz);
		} catch (IOException e) {
			throw new RuntimeException("Problem deserializing w2vlda model",e);
		}
	}
	
//	public SerializableModel deserialize(){
//		try {
//			return SerializationUtils.deserialize(new FileInputStream(w2vldaModelPath));
//		} catch (FileNotFoundException e) {
//			throw new RuntimeException("Problem deserializing w2vlda model",e);
//		}
//	}
//	
//	public void serialize(SerializableModel model){
//		try {
//			SerializationUtils.serialize(model,new FileOutputStream(w2vldaModelPath));
//		} catch (FileNotFoundException e) {
//			throw new RuntimeException("Problem serializing w2vlda model",e);
//		}
//	}
	
}
