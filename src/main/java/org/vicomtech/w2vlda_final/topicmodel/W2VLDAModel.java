package org.vicomtech.w2vlda_final.topicmodel;

import java.io.Serializable;

import org.vicomtech.w2vlda_final.datasets.state.TopicMapping;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;

public class W2VLDAModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double[][][] topicWordtypeWordCount;
	private double[][] topicWordtypeWordSum;
	private Vocabulary vocabulary;
	private TopicMapping topicMapping;
	private TopicsConfiguration topicsConfiguration;


	public double[][][] getTopicWordtypeWordCount() {
		return topicWordtypeWordCount;
	}

	public void setTopicWordtypeWordCount(double[][][] topicWordtypeWordCount) {
		this.topicWordtypeWordCount = topicWordtypeWordCount;
	}

	public double[][] getTopicWordtypeWordSum() {
		return topicWordtypeWordSum;
	}

	public void setTopicWordtypeWordSum(double[][] topicWordtypeWordSum) {
		this.topicWordtypeWordSum = topicWordtypeWordSum;
	}

	public Vocabulary getVocabulary() {
		return vocabulary;
	}

	public void setVocabulary(Vocabulary vocabulary) {
		this.vocabulary = vocabulary;
	}

	public TopicMapping getTopicMapping() {
		return topicMapping;
	}

	public void setTopicMapping(TopicMapping topicMapping) {
		this.topicMapping = topicMapping;
	}

	public TopicsConfiguration getTopicsConfiguration() {
		return topicsConfiguration;
	}

	public void setTopicsConfiguration(TopicsConfiguration topicsConfiguration) {
		this.topicsConfiguration = topicsConfiguration;
	}	

}
