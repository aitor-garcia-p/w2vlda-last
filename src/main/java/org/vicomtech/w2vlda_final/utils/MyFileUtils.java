package org.vicomtech.w2vlda_final.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyFileUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(MyFileUtils.class);
	
	public static List<String>readLines(String path){
		try {
			LOGGER.info("Reading lines from: {}",path);
			return FileUtils.readLines(new File(path), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void writeLines(List<String>lines,String path){
		try {
			FileUtils.writeLines(new File(path),"UTF-8", lines);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String writeLinesToTempFileAndRetrievePath(List<String>lines){
		try {
			File temp=File.createTempFile("aaa", "temp");
			temp.deleteOnExit();
			FileUtils.writeLines(temp,"UTF-8", lines);
			return temp.getAbsolutePath();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String appendParamsToFileNameBeforeItsExtension(String fileName, String ... params){
		String partBeforeExtension=fileName.substring(0,fileName.lastIndexOf('.'));
		String extension=fileName.substring(fileName.lastIndexOf('.'));
		StringBuilder sb=new StringBuilder();
		for(String param:params){
			sb.append(param);
			sb.append("_");
		}
		String paramsStr=sb.toString();
		paramsStr=paramsStr.substring(0, paramsStr.lastIndexOf('_'));
		return partBeforeExtension+"_"+paramsStr+extension;
	}
	
}
