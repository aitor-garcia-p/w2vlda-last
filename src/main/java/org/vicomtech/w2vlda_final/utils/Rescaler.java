package org.vicomtech.w2vlda_final.utils;

public class Rescaler {

	private double originalMinimum;
	private double originalMaximum;
	private double newMininum;
	private double newMaximum;

	public Rescaler(double originalMinimum, double originalMaximum, double newMininum, double newMaximum) {
		super();
		this.originalMinimum = originalMinimum;
		this.originalMaximum = originalMaximum;
		this.newMininum = newMininum;
		this.newMaximum = newMaximum;
	}

//	public static double renormalizeCosSim(double cos) {
//		return (cos + 1) / 2;
//	}

	public static void main(String[] args) {
		Rescaler rescaler=new Rescaler(-1.0, 1.0, 0.0, 1.0);
		double cos1 = -1;
		double cos2 = 1;
		double ren1 = rescaler.rescale(cos1);//renormalizeCosSim(cos1);
		double ren2 = rescaler.rescale(cos2);//renormalizeCosSim(cos2);

		System.out.println(cos1 + " " + cos2 + " " + ren1 + " " + ren2);
	}

	public double rescale(double value) {
		double scale = (newMaximum - newMininum) / (originalMaximum - originalMinimum);
		return (newMininum + ((value - originalMinimum) * scale));
	}

}
