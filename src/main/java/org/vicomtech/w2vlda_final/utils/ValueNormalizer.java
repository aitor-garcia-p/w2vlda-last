package org.vicomtech.w2vlda_final.utils;

import java.util.List;
import java.util.Map;

import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.datasets.state.Word;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ValueNormalizer {

	public static List<ValueComparablePair>normalizeZeroToOne(List<ValueComparablePair>pairs){
		double min=Double.MAX_VALUE;
		double max=Double.MIN_VALUE;
		for(ValueComparablePair pair:pairs){
			if(pair._2>max){
				max=pair._2;
			}
			if(pair._2<min){
				min=pair._2;
			}
		}
		List<ValueComparablePair>normalizedPairs=Lists.newArrayList();
		for(ValueComparablePair pair:pairs){
			if(max-min==0){//all values are equal, return 0.5
				normalizedPairs.add(new ValueComparablePair(pair._1, 0.5));
			}else{
				normalizedPairs.add(new ValueComparablePair(pair._1, (pair._2-min)/(max-min)));
			}
			
		}
		return normalizedPairs;
	}
	
	public static double[][][]normalizePhiByWordsFrequency(double[][][]phi,Dataset dataset,Vocabulary vocabulary){
		Map<String, Double> wordFrequencies = countWordFrequency(dataset);
		double[][][]phiNorm=new double[phi.length][phi[0].length][phi[0][0].length];
		for(int topic=0;topic<phi.length;topic++){
			for(int wordType=0;wordType<phi[0].length;wordType++){
				for(int wordId=0;wordId<vocabulary.size();wordId++){
					if(wordFrequencies.containsKey(vocabulary.getWord(wordId))){//this is not correct, because every word should be there, and be normalized... the "510" gives null, I don not know why
						//System.out.println("vocabulary word "+wordId+" --> "+vocabulary.getWord(wordId)+ " wordFreq: "+wordFrequencies.get(vocabulary.getWord(wordId)));
						phiNorm[topic][wordType][wordId]=phi[topic][wordType][wordId]/wordFrequencies.get(vocabulary.getWord(wordId));
					}
				}
			}
		}
		return phiNorm;
	}
	
	public static Map<String,Double> countWordFrequency(Dataset dataset){
		Map<String,Double>counts=Maps.newHashMap();
		for(Document document:dataset.getDocuments()){
			for(Word word:document.getWords()){
				double value=counts.getOrDefault(word.getWordform(),0.0);
				value++;
				counts.put(word.getWordform(), value);
			}
		}
		return counts;
	}
	
}
