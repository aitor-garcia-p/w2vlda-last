package org.vicomtech.w2vlda_final.utils;

public class ValueComparablePair implements Comparable<ValueComparablePair>{
	
	public final String _1;
	public final double _2;
	
	public ValueComparablePair(String _1, double _2) {
		super();
		this._1 = _1;
		this._2 = _2;
	}

	@Override
	public int compareTo(ValueComparablePair o) {
		//System.err.println("Inside ValuComparablePair, comparing "+this._1+":"+this._2+" to "+o._1+":"+o._2);
		if(this._2>o._2){
			return -1;
		} else if(this._2==o._2){
			return this._1.compareTo(o._1);
		}else {
			return 1;
		}
	}
}
