package org.vicomtech.w2vlda_final.utils;

public class TextBasicPreprocess {

	private static final String MULTIWORD_MARKER = "#";
	private static final String NEGATION_JOINER="_";

	public static String preprocess(String text,boolean removeTildes){
		String preprocessedText=text;
		if(removeTildes){
			preprocessedText=replaceTildes(preprocessedText);
		}
		return preprocessedText.toLowerCase().replaceAll("([\\p{Punct}&&[^"+MULTIWORD_MARKER+"]&&[^"+NEGATION_JOINER+"]]+)", " $1 ").replaceAll("\\s+", " ").trim();
	}
	
	private static String replaceTildes(String text){
		return text.replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o").replace("ú", "u");
	}
	
}
