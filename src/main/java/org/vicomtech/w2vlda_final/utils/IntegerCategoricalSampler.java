package org.vicomtech.w2vlda_final.utils;

import java.util.Random;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.hash.TIntDoubleHashMap;

/**
 * 
 * Class to allow for probability sampling from categories, each with a defined
 * probability of selection. Items are added
 * 
 * to the sampler with a given sampling value - either the default supplied -
 * and this value is used to sample
 * 
 * from the underlying items in a probabilistic way. The true probability of
 * selection is the defined sampling value
 * 
 * of an item divided by the total sampling value of all items.
 * 
 * @author Greg Cope
 *
 * 
 * 
 * @param <T>
 * 
 */

public class IntegerCategoricalSampler {

	private TIntDoubleHashMap samplingValues = new TIntDoubleHashMap();

	private double defaultSamplingValue;

	private double totalSamplingValue = 0;

	/**
	 * 
	 * Constructs a new sampler object with a default probability of added items
	 * of 0.1
	 * 
	 */

//	private CategoricalSampler() {
//
//		this(0.1);
//		this.random=new Random();
//	}
	
	private Random random;
	//Additional constructor to inject Random
	public IntegerCategoricalSampler(Random random) {

		this(0.1);
		this.random=random;
	}
	
	//

	/**
	 * 
	 * Constructs a new sampler object with the defined default probability.
	 * 
	 * @param defaultSamplingValue
	 * 
	 */

	private IntegerCategoricalSampler(double defaultSamplingValue) {

		this.defaultSamplingValue = defaultSamplingValue;

	}

	/**
	 * 
	 * Adds the given parameter item, using the default probability value.
	 * 
	 * @param item
	 * 
	 */

	public void addItem(int item) {

		addItem(item, defaultSamplingValue);

	}

	/**
	 * 
	 * Adds the given item to this sampler with the given value. If
	 * 
	 * the item already exists, it is replaced with the given value
	 * 
	 * @param item
	 * 
	 * @param probability
	 * 
	 */

	public void addItem(int item, double value) {

		if (!samplingValues.containsKey(item)) {

			samplingValues.put(item, value);

			totalSamplingValue += value;

		} else {

			Double d = samplingValues.get(item);

			totalSamplingValue -= d;

			samplingValues.put(item, value);

			totalSamplingValue += value;

		}

	}

	/**
	 * 
	 * Increments an items value by incrementor. If the value does
	 * 
	 * not exist within this sampler, it is added with incrementor inital value
	 * 
	 * @param item
	 * 
	 * @param incrementor
	 * 
	 */

	public void incrementSamplingValue(int item, double incrementor) {

		if (!samplingValues.containsKey(item)) {

			addItem(item, incrementor);

		} else {

			double val = samplingValues.get(item);

			samplingValues.put(item, val + incrementor);

			totalSamplingValue += incrementor;

		}

	}

	/**
	 * 
	 * Returns the sampling value of the parameter item, or -1 if the item is
	 * not part of this CategoricalSampler.
	 * 
	 * @param item
	 * 
	 * @return
	 * 
	 */

	public double getSamplingValue(int item) {

		if (!samplingValues.containsKey(item)) {

			return -1;

		}

		return samplingValues.get(item);

	}

	/**
	 * 
	 * Sets the sampling value of the given item.
	 * 
	 * @param item
	 * 
	 * @param probability
	 * 
	 */

	public void setSamplingValue(int item, double value) {

		addItem(item, value);

	}

	/**
	 * 
	 * Samples a discrete value from the underlying distribution. This method
	 * runs in linear time
	 * 
	 * @return The sampled item.
	 * 
	 */

	public int sample() {

		if (samplingValues.size() == 0) {

			throw new IllegalStateException("Empty set");

		}

//		double val = Math.random() * totalSamplingValue;
		double val = random.nextDouble() * totalSamplingValue;

		TIntIterator items = samplingValues.keySet().iterator();//.keySet().iterator();

		double total = 0;

		int returner = -1;

		while (items.hasNext()) {

			int item = items.next();

			double prop = samplingValues.get(item);

			if (val >= total && val < total + prop) {

				returner = item;

				break;

			}

			total += prop;

		}

		return returner;

	}

	@Override

	public String toString() {

		return samplingValues.toString();

	}
	
	public void clearSamplerValues(){
		this.samplingValues.clear();
		this.totalSamplingValue=0.0;
	}

}