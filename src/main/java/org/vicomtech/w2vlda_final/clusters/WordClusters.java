package org.vicomtech.w2vlda_final.clusters;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.google.common.collect.Maps;

public class WordClusters {

	public static final char BROWN_CLUSTER='b';
	public static final char WORDVECTORS_CLUSTER='v';
	
	private static final String OOV_CLUSTER="OOV";
	
	public static WordClusters loadBrownClusters(String path){
		return new WordClusters(BROWN_CLUSTER, path);
	}
	
	public static WordClusters loadWordVectorsClusters(String path){
		return new WordClusters(WORDVECTORS_CLUSTER, path);
	}
	
	private final char clusterType;
	
	private Map<String,String>wordClusterMap=Maps.newHashMap();
	
	private WordClusters(char clusterType, String path){
		this.clusterType=clusterType;
		loadWordClusterMap(path);
	}

	private void loadWordClusterMap(String path) {
		List<String>lines=readFile(path);
		lines.stream().forEach(x->processLine(x));
	}
	
	public String getCluster(String word){
		return wordClusterMap.getOrDefault(word, OOV_CLUSTER);
	}
	
	private List<String>readFile(String path){
		try {
			return FileUtils.readLines(new File(path), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException("Problem reading Clusters file at "+path,e);
		}
	}
	
	/**
	 * Processes the line to feed the wordCluster map. 
	 * In addition it could filter out words that appear less than a certain threshold, parsing the third column.
	 * @param line
	 */
	private void processLine(String line){
		if(!line.trim().isEmpty()){
			String[]parts=line.split("\t");
			String cluster=parts[0];
			String word=parts[1];
			wordClusterMap.put(word, cluster);
		}
	}
	
	public char getClusterType() {
		return clusterType;
	}
	
}
