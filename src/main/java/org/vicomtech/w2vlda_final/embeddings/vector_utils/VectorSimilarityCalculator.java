package org.vicomtech.w2vlda_final.embeddings.vector_utils;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vicomtech.w2vlda_final.utils.Rescaler;

/**
 * A helper class to calculate vector based similarities
 * @author yo
 *
 */
public class VectorSimilarityCalculator {

	@SuppressWarnings("unused")
	private static final Logger logger=LoggerFactory.getLogger(VectorSimilarityCalculator.class);
	//private static final int SIMILARITY_METRIC=Similarity.COSINE;
	
	private static final double SMALL_NON_ZERO_VALUE_FOR_NULLS=0.01;

	
	//Used for differential similarity
	private Rescaler minus2plus2zeroToOneRescaler=new Rescaler(-2, 2, 0, 1);
	private Rescaler minus1plus1zeroToOneRescaler=new Rescaler(-1, 1, 0, 1);


	public VectorSimilarityCalculator(){
		super();
	}
	
	public void init(){
		//no-op
	}
	
	public double calculateSimilarity(double[]vector1,double[]vector2){
		if(vector1==null || vector2==null){
			return SMALL_NON_ZERO_VALUE_FOR_NULLS;
		}else{
			//return rescaler.rescale(Similarity.calculateSimilarity(vector1, vector2, SIMILARITY_METRIC));
			return Similarity.calculateSimilarity(vector1, vector2, Similarity.COSINE);
		}
	}
	
	/**
	 * Returns the maximum cosine similarity value, rescaled from [-1,1] to [0,1]
	 * @param vector1
	 * @param vectors
	 * @return
	 */
	public double calculateMaxSimilarity(double[]vector1,List<double[]>vectors){
		if(vectors.isEmpty()){
			return SMALL_NON_ZERO_VALUE_FOR_NULLS;
		}else{
			double currentMax=Double.NEGATIVE_INFINITY;
			for(double[]vector:vectors){
				double similarity=calculateSimilarity(vector1, vector);
				currentMax=Math.max(currentMax, similarity);
			}
			return minus1plus1zeroToOneRescaler.rescale(currentMax);// Math.max(currentMax, 0.0);
		}
	}
	
	/**
	 * Returns the average similarity between a given vectors and a list of other vectors
	 * @param vector1
	 * @param vectors
	 * @return
	 */
	public double calculateAverageSimilarity(double[]vector1,List<double[]>vectors){
		if(vectors.isEmpty()){
			return SMALL_NON_ZERO_VALUE_FOR_NULLS;
		}else{
			double accum=0.0;
			for(double[]vector:vectors){
				accum+=minus1plus1zeroToOneRescaler.rescale(calculateSimilarity(vector1, vector));
			}
			return accum/(double)vectors.size();
		}
	}
	
	/**
	 * Returns the maximum cosine similarity to positives minus the maximum cosine similarity to negatives, rescaled from [-2,2] to [0,1]
	 * @param vector1
	 * @param vectors1
	 * @param vectors2
	 * @return
	 */
	public double calculateDiferentialSimilarity(double[]vector1,List<double[]>vectors1,List<double[]>vectors2){
		double max1=calculateMaxSimilarity(vector1, vectors1);
		double max2=calculateMaxSimilarity(vector1, vectors2);
		return minus2plus2zeroToOneRescaler.rescale(max1-max2);
	}
	
}
