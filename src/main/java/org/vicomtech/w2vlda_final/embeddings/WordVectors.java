package org.vicomtech.w2vlda_final.embeddings;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * A class to hold word vectors loaded from a plain text file with format:
 * word1 v11 v12 v13 ... v1N
 * word2 v21 v22 v23 ... v2N
 * ...
 * @author yo
 *
 */
public class WordVectors {

	private static final Logger LOGGER = LoggerFactory.getLogger(WordVectors.class);
	
	private static final String DEFAULT_SEPARATOR=" ";
	
	private Map<String,double[]>wordVectors;
	
	public WordVectors(String pathToVectorsFile){
		this.wordVectors=loadWordVectors(pathToVectorsFile);
	}
	
	public double[]getWordVector(String word){
		return this.wordVectors.get(word);
	}
	
	public List<double[]>getWordVectors(List<String>words){
		List<double[]>wordVectors=Lists.newArrayList();
		for(String word:words){
			wordVectors.add(getWordVector(word));
		}
		return wordVectors;
	}
	
	public List<String>getWords(){
		return Lists.newArrayList(wordVectors.keySet());
	}
	
	public boolean containsWord(String word){
		return wordVectors.containsKey(word);
	}

	
	public static Map<String,double[]>loadWordVectors(String path){
		LOGGER.info("Loading word vectors from: "+path);
		return loadWordVectors(path, DEFAULT_SEPARATOR);
	}
	
	public static Map<String,double[]>loadWordVectors(String path,String separator){
		try {
			return loadWordVectors(new FileInputStream(path),separator);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Map<String,double[]>loadWordVectors(InputStream is,String separator){
		try {
			List<String>lines=IOUtils.readLines(is, StandardCharsets.UTF_8);
			Map<String,double[]>wordVectors=Maps.newHashMap();
			for(String line:lines){
				String[]wordAndVector=line.split(separator, 2);
				//wordnet based embeddings use undescore for composed words, change to '#'
				String word=wordAndVector[0].replace("_", "#");
				double[]vector=convertToDoubleArray(wordAndVector[1].split(separator));
				wordVectors.put(word, vector);
			}
			return wordVectors;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	private static double[]convertToDoubleArray(String[]vectorOfStrings){
		double[]vectorOfDoubles=new double[vectorOfStrings.length];
		for(int i=0;i<vectorOfDoubles.length;i++){
			vectorOfDoubles[i]=Double.parseDouble(vectorOfStrings[i]);
		}
		return vectorOfDoubles;
	}
	
}
