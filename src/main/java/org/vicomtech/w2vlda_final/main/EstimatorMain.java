package org.vicomtech.w2vlda_final.main;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPOutputStream;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.TopicMapping;
import org.vicomtech.w2vlda_final.datasets.state.Vocabulary;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.W2VLDAModel;
import org.vicomtech.w2vlda_final.topicmodel.estimation.BaseSpringConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.estimation.Estimator;
import org.vicomtech.w2vlda_final.topicmodel.resultprinting.TopicModelPrinter;
import org.vicomtech.w2vlda_final.topicmodel.resultprinting.W2VLDAModelSerializer;
import org.vicomtech.w2vlda_final.utils.ValueNormalizer;
public class EstimatorMain {

	/**
	 * Main class to invoke the full process. For now the only admitted parameter is the configuration file path
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {

		String configFilePath="";
		if(args.length!=1){
			configFilePath="C:\\Users\\yo\\Dropbox\\PhD\\FINAL_EXPERIMENTS_STUFF\\W2VLDA_FINAL_ASSETS\\run_configuration.properties";
			//throw new RuntimeException("Bad number of parameters, it needs the path to the configuration file");
		}else{
			configFilePath=args[0];
		}
		
		PropertySourcesPlaceholderConfigurer ppc = propertyConfigInDev(configFilePath);
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(BaseSpringConfiguration.class);
		context.addBeanFactoryPostProcessor(ppc);
		context.refresh();

		
		Vocabulary vocabulary = context.getBean(Vocabulary.class);
		TopicMapping topicMapping = context.getBean(TopicMapping.class);
		Dataset dataset = context.getBean(Dataset.class);

		//// Estimation process
		Estimator estimator = context.getBean(Estimator.class);
		estimator.estimate();
		/////////////////////////

		TopicModelPrinter topicModelPrinter = new TopicModelPrinter(vocabulary, topicMapping, 30);
		double[][][] phiNormalizedByWordFreq = ValueNormalizer.normalizePhiByWordsFrequency(estimator.getPhi(), dataset,
				vocabulary);
		topicModelPrinter.printWordDistributions(phiNormalizedByWordFreq, System.out);
		topicModelPrinter.printTopDocumentsPerTopics(dataset, estimator.getTheta(), System.out);
		topicModelPrinter.printTopDocumentsPerPolarity(dataset, estimator.getOmega(), System.out);

		////
		// PRINT ALSO TO A FILE
		String resultPrintingPath = context.getBeanFactory()
				.resolveEmbeddedValue("${w2vlda.w2vlda_resultPrintingFile}");
		DateFormat df=new SimpleDateFormat("YYYYMMDD_HHmm");
		resultPrintingPath=resultPrintingPath.replaceAll("(\\.\\w+)$", "_"+df.format(new Date())+"$1");
		/////// Do not compress the printed output file
		boolean compress=false;
		//////
		PrintStream printStream;
		if(compress){
			printStream=new PrintStream(new GZIPOutputStream(new FileOutputStream(resultPrintingPath+".gzip")));
		}else{
			printStream=new PrintStream(new FileOutputStream(resultPrintingPath));
		}
		topicModelPrinter.printWordDistributions(phiNormalizedByWordFreq, printStream);
		topicModelPrinter.printTopDocumentsPerTopics(dataset, estimator.getTheta(), printStream);
		topicModelPrinter.printTopDocumentsPerPolarity(dataset, estimator.getOmega(), printStream);
		
		printStream.close();
		////////////////////
		TopicsConfiguration topicsConfiguration = context.getBean(TopicsConfiguration.class);
		W2VLDAModel model = new W2VLDAModel();
		// model.setDocumentTopics(estimator.getDocumentTopicsCount());
		model.setTopicWordtypeWordCount(estimator.getTopicWordtypeWordCount());
		model.setTopicWordtypeWordSum(estimator.getTopicWordtypeWordSum());
		model.setVocabulary(vocabulary);
		model.setTopicMapping(topicMapping);
		model.setTopicsConfiguration(topicsConfiguration);
		String modelPath = context.getBeanFactory().resolveEmbeddedValue("${w2vlda.w2vlda_model}");
		//DateFormat df=new SimpleDateFormat("YYYYMMDD_HHmm");
		modelPath=modelPath.replaceAll("(\\.\\w+)$", "_"+df.format(new Date())+"$1");
		W2VLDAModelSerializer.serialize(model, modelPath);
		///
		context.close();
	}

	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev(String path) {
		PropertySourcesPlaceholderConfigurer propSource = new PropertySourcesPlaceholderConfigurer();
		ClassPathResource cpr = new ClassPathResource(path);
		if (cpr.exists()) {
			propSource.setLocation(cpr);
		} else {
			FileSystemResource fsr = new FileSystemResource(path);
			propSource.setLocation(fsr);
		}
		return propSource;
	}

}
