package org.vicomtech.w2vlda_final.maxent;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.collect.Maps;

import opennlp.tools.ml.maxent.GIS;
import opennlp.tools.ml.maxent.GISModel;
import opennlp.tools.ml.maxent.io.GISModelReader;
import opennlp.tools.ml.maxent.io.GISModelWriter;
import opennlp.tools.ml.maxent.io.SuffixSensitiveGISModelReader;
import opennlp.tools.ml.maxent.io.SuffixSensitiveGISModelWriter;
import opennlp.tools.ml.model.FileEventStream;
import opennlp.tools.ml.model.OnePassDataIndexer;

/**
 * This class embeds OpenNLP maxent classifier
 * @author yo
 *
 */
public class MaxEntClassifier {

	private static final Logger LOGGER=LoggerFactory.getLogger(MaxEntClassifier.class);
	
	@Value("${w2vlda.maxent_model}")
	private String maxEntModelPath;
	
	//STATE VARIABLES
	private GISModel model;
	
	public MaxEntClassifier(){
		super();
	}
	
	public MaxEntClassifier(String maxEntModelPath){
		this.maxEntModelPath=maxEntModelPath;
		init();
	}
	
	public void init(){
		try {
			GISModelReader gisModelReader=new SuffixSensitiveGISModelReader(new File(maxEntModelPath));
			this.model= (GISModel) gisModelReader.constructModel();
		} catch (IOException e) {
			throw new RuntimeException("Problem reading MaxEnt model from : "+maxEntModelPath,e);
		}
	}
	
	public Map<String,Double> evaluateInstance(String[]features){
		if(model==null){
			LOGGER.error("MaxEnt model is null. Train it first.");
			throw new RuntimeException("MaxEnt model is null");
		}
		double[] results = model.eval(features);
		Map<String,Double>resultMap=Maps.newHashMap();
		for(int i=0;i<model.getNumOutcomes();i++){
			resultMap.put(model.getOutcome(i), results[i]);
		}
		return resultMap;
	}
	
	/**
	 * Careful with this, I am skipping the hashmap, but now the indexes of the outcomes are not controlled, they are usually 0=AT, 1=OW, but they may change
	 * @param features
	 * @return
	 */
	public Map<String, Double> evaluateInstance2(String[]features){
		if(model==null){
			LOGGER.error("MaxEnt model is null. Train it first.");
			throw new RuntimeException("MaxEnt model is null");
		}
		double[] results = model.eval(features);
		Map<String, Double> resultMap = Maps.newHashMap();
		for (int i = 0; i < model.getNumOutcomes(); i++) {
			resultMap.put(model.getOutcome(i), results[i]);
		}
		return resultMap;
		//return results;
	}
	
	public void trainMaxEntModel(String pathToTrainingFile, int numIters) {
		GISModel model;
		FileEventStream rvfes1 = null;
		try {
			rvfes1 = new FileEventStream(pathToTrainingFile);
			model = GIS.trainModel(numIters, new OnePassDataIndexer(rvfes1, 1));
			this.model=model;
		} catch (IOException e) {
			throw new RuntimeException("Problem readin MaxEnt training file", e);
		} finally {
			try {
				rvfes1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Saves the model. Tries to fix automatically the weird problem about the "GIS" at the beginning
	 * @param path
	 */
	public void save(String path){
		try {
			File tempFile=File.createTempFile("temp", "temp");
			tempFile.deleteOnExit();
			GISModelWriter gisModelWriter=new SuffixSensitiveGISModelWriter(model, tempFile);
			gisModelWriter.persist();
			List<String> lines = FileUtils.readLines(tempFile,StandardCharsets.UTF_8);
			if(!lines.remove(0).equals("GIS")){
				LOGGER.warn("Removed line in MaxEnt serialized model is not 'GIS'!");
			}
			FileUtils.writeLines(new File(path),"UTF-8", lines);
			//gisModelWriter.close();
		} catch (IOException e) {
			throw new RuntimeException("Error saving MaxEnt model",e);
		}
	}
	
}
