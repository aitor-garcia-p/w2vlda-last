package org.vicomtech.w2vlda_final.maxent;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vicomtech.w2vlda_final.clusters.WordClusters;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.datasets.state.Document;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * A helper class for maxent training set generator (bootstrapping seed word occurrences and contexts)
 * @author yo
 *
 */
public class MaxEntTrainingSetGenerator {

	@SuppressWarnings("unused")
	private static final Logger logger=LoggerFactory.getLogger(MaxEntTrainingSetGenerator.class);
	
	private static final String OTHER_WORD_LABEL = "OTHER";
	private static final String ASPECT_TERM_LABEL = "ASPECT_TERM";
	private static final String OPINION_WORD_LABEL = "OPINION_WORD";

	private static final String PADDING_MARKER = "PADDING";
	private static final int WINDOW = 2;

	public List<String> generateTrainSet(Set<String> aspectTermSeeds, Set<String> opinionWordSeeds,
			List<WordClusters> wordClustersToBeUsedForFeatures, Dataset dataset, int maxTrainingInstances) {
		//List<String> trainInstances = Lists.newArrayList();
		Map<String,List<String>>labelInstancesMap=Maps.newHashMap();
		for (Document document : dataset.getDocuments()) {
			for (int i = 0; i < document.size(); i++) {
				String wordForm=document.getWords().get(i).getWordform();
				if (aspectTermSeeds.contains(wordForm) || opinionWordSeeds.contains(wordForm)) {
					String featuresString = generateFeatureString(wordClustersToBeUsedForFeatures, document, i);
					String label = generateInstanceLabel(aspectTermSeeds, opinionWordSeeds, document, i);
					//trainInstances.add(label + " " + featuresString);
					List<String>labelInstances=labelInstancesMap.getOrDefault(label,Lists.newArrayList());
					labelInstances.add(label + " " + featuresString);
					labelInstancesMap.put(label, labelInstances);
				}
			}
		}
		List<String>balancedTrainingInstances=balanceAndLimitTrainingInstances(labelInstancesMap, maxTrainingInstances);
		return balancedTrainingInstances;
	}

	private List<String>balanceAndLimitTrainingInstances(Map<String,List<String>>labelInstancesMap,int maxTrainingInstancesPerLabel){
		int min=Integer.MAX_VALUE;
		for(String label:labelInstancesMap.keySet()){
			int labelInstancesSize=labelInstancesMap.get(label).size();
			if(labelInstancesSize<min){
				min=labelInstancesSize;
			}
		}
		List<String>balancedTrainingInstances=Lists.newArrayList();
		for(String label:labelInstancesMap.keySet()){
			balancedTrainingInstances.addAll(labelInstancesMap.get(label).subList(0, Math.min(min,maxTrainingInstancesPerLabel)));
		}
		return balancedTrainingInstances;
	}
	
	private String generateInstanceLabel(Set<String> aspectTerms, Set<String> positivesAndNegatives, Document document,
			int i) {
		String label = OTHER_WORD_LABEL;
		if (aspectTerms.contains(document.getWords().get(i).getWordform())) {
			label = ASPECT_TERM_LABEL;
		} else if (positivesAndNegatives.contains(document.getWords().get(i).getWordform())) {
			label = OPINION_WORD_LABEL;
		}
		return label;
	}

	public String generateFeatureString(List<WordClusters> wordClustersList, Document document, int i) {
		StringBuilder sb = new StringBuilder(85);
		for (WordClusters wordClusters : wordClustersList) {
			generateFeatureStringFast(sb, wordClusters, document, i);
		}
		return sb.toString().trim();
	}

	public String generateFeatureString(WordClusters wordClusters, Document document, int i) {
		StringBuilder sb = new StringBuilder();
		generateFeatureStringFast(sb, wordClusters, document, i);
		return sb.toString().trim();
	}

	public void generateFeatureStringFast(StringBuilder sb, WordClusters wordClusters, Document document, int i) {
		for (int j = i - WINDOW; j <= i + WINDOW; j++) {
			sb.append(wordClusters.getClusterType());
			sb.append(j - i);
			sb.append('=');
			if (j < 0 || j > document.size() - 1) {
				sb.append(PADDING_MARKER);
			} else {
				sb.append(wordClusters.getCluster(document.getWords().get(j).getWordform()));
			}
			sb.append(' ');
		}
	}

	///////
	/// FASTER
	//////
	//NOTE: the empty string preceding the features composition is because otherwise the 'char' elements become integers...
	public String[] generateFeatureString2(List<WordClusters> wordClustersList, Document document, int i) {
		
		String[] features = new String[WINDOW * 2 + 1];// pre+pos+self
		int featureAbsIndex = 0;
		for (WordClusters wordClusters : wordClustersList) {
			for (int j = i - WINDOW; j <= i + WINDOW; j++) {
				if (j < 0 || j > document.size() - 1) {
					features[featureAbsIndex] = ""+wordClusters.getClusterType() + (j - i) + '=' + PADDING_MARKER;
				} else {
					features[featureAbsIndex] = ""+wordClusters.getClusterType() + (j - i) + '='
							+ wordClusters.getCluster(document.getWords().get(j).getWordform());
				}
				//logger.info("Added feature {}",features[featureAbsIndex]);
				featureAbsIndex++;
			}
		}
		return features;

	}

}
