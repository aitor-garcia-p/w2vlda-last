package org.vicomtech.w2vlda_final.maxent;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vicomtech.w2vlda_final.clusters.WordClusters;
import org.vicomtech.w2vlda_final.datasets.state.Dataset;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration;
import org.vicomtech.w2vlda_final.topicmodel.TopicsConfiguration.Topic;
import org.vicomtech.w2vlda_final.utils.MyFileUtils;

import com.google.common.collect.Sets;

/**
 * A helper class to execute the OpenNLP maxent classifier model training
 * @author yo
 *
 */
public class AspectOpinionMaxEntModelGenerator {

	private static final Logger logger=LoggerFactory.getLogger(AspectOpinionMaxEntModelGenerator.class);
	
	private List<WordClusters> wordClusters;

	public AspectOpinionMaxEntModelGenerator(List<WordClusters> wordClusters) {
		this.wordClusters = wordClusters;
	}

	/**
	 * With the given dataset and topic configuration seeds bootstraps training
	 * instances modeled with the given word clusters. Then it trains a maxent
	 * model and writes it out to the given path.
	 * 
	 * @param dataset
	 * @param topicConfiguration
	 * @param outputModelPath
	 */
	public void generateAndTrainAspectOpinionMaxEnt(Dataset dataset, TopicsConfiguration topicConfiguration,int maxTrainingInstances, int numMaxEntTrainIters,
			String outputModelPath) {
		Set<String> aspectTerms = Sets.newHashSet();
		Set<String> opinionWords = Sets.newHashSet();
		for (Topic topic : topicConfiguration.getTopics()) {
			aspectTerms.addAll(topic.getAspectTerms());
			opinionWords.addAll(topic.getPositives());
			opinionWords.addAll(topic.getNegatives());
		}
		opinionWords.addAll(topicConfiguration.getGeneralPositives());
		opinionWords.addAll(topicConfiguration.getGeneralNegatives());
		
		logger.info("Aspect term seeds: {}",aspectTerms);
		logger.info("Opinion word seeds: {}",opinionWords);
		MaxEntTrainingSetGenerator maxEntTrainingSetGenerator = new MaxEntTrainingSetGenerator();
		List<String> trainingInstances = maxEntTrainingSetGenerator.generateTrainSet(aspectTerms, opinionWords,
				wordClusters, dataset,maxTrainingInstances);
		logger.info("Generated {} training instances",trainingInstances.size());
		String trainingInstancesFilePath = MyFileUtils.writeLinesToTempFileAndRetrievePath(trainingInstances);
		MaxEntClassifier maxEntClassifier = new MaxEntClassifier();
		maxEntClassifier.trainMaxEntModel(trainingInstancesFilePath,numMaxEntTrainIters);
		maxEntClassifier.save(outputModelPath);
	}

}
