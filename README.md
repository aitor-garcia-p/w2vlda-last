## W2VLDA: weakly supervised aspect based sentiment analysis based on word embeddings and topic modelling

**Note: this code is for personal experiments and shown here mainly for illustrative purposes. It is a code migration/cleaning from another codebase and may lack some features or may not work properly for now.**

The source code contained in this repository can be packaged using Apache Maven.
After cloning the repository and navigating to the root folder (the one that contains the pom.xml file) issue a **mvn clean package** command.
If the command completes successfully a jar file will be created in the target subfolder.

In order to run the model estimation run:

```
java -cp PATH_TO_JAR_FILE org.vicomtech.w2vlda_final.main.EstimatorMain PATH_TO_CONFIG_FILE
```

There is an example configuration file in the Downloads section of this repository in Bitbucket.
The file has comments explaining each configuration variable.
There is also a (domain aspect) topic configuration example file in the downloads section of this bitbucket repository.

Together with the topic configuration, the most relevant ones are the other resources neccessary to make the process work: pre-computed word embeddings (both from the target domain and from a general domain), brown clusters and aspect-term and opinion-word separation MaxEnt classifier.

In order to compute word embeddings any approach or software can be used (e.g. any Word2Vec implementation), but the resulting word-vectors must be in plain text, one word per line, and each vector component separated by a white space:
```
[...]
waiter -0.089069753 -0.40525740 0.1692177206 0.197918 0.0416115 [...]
waitress -0.1088036 -0.33540585 0.1266988 0.223027 0.1448615 [...]
waitstaff -0.08187632 -0.076069131 -0.011193322 0.05211345 0.118546478 [...]
[...]
```

The brown clusters file must be in the following format:
```
1111001111	bartender	277
1111001111	owner	278
1111001111	bellman	292
1111001111	girl	335
1111001111	waiter	451
```

Where the first column is the cluster identifier, the second column is the word itself. The third column is optional (it is the frequency of the word in the processed corpus).
This is the output format that directly emerges from the Brown clustering implementation we have used. It can be found at https://github.com/koendeschacht/brown-cluster

**TODO: complete the documentation**